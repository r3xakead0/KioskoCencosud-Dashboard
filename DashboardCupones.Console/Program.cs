﻿using System;
using Util;
using BL = DashboardCupones.Library.BusinessLogic;
using System.Threading.Tasks;
using System.Threading;

namespace DashboardCupones.Console
{
    class Program
    {

        const int PORT_NO = 9876;

        static int waitMinutes = 1;
        static DateTime lastUpdated = DateTime.Now;

        static void Main(string[] args)
        {
            try
            {

                var bl = new BL.Kiosko();
                lastUpdated = bl.UltimoMonitoreo();

                while (true)
                {
                    try
                    {
                        var diffMinutes = (DateTime.Now - lastUpdated).Minutes;

                        if (diffMinutes >= waitMinutes)
                        {
                            var tsk = Task.Run(() => bl.Registrar());
                            tsk.Wait();
                            lastUpdated = tsk.Result;

                            var lstKioskos = bl.Monitoreo(lastUpdated);

                            var table = lstKioskos.ToStringTable(
                                            u => u.Empresa,
                                            u => u.Tienda,
                                            u => u.Hostname,
                                            u => u.Conexion,
                                            u => u.Version,
                                            u => u.Sesion,
                                            u => u.Impreso,
                                            u => u.Restante
                                        );

                            System.Console.Clear();

                            System.Console.BackgroundColor = ConsoleColor.DarkGreen;
                            System.Console.ForegroundColor = ConsoleColor.White;
                            System.Console.WriteLine(CentrarTexto("MONITOREO DE KIOSKOS", System.Console.WindowWidth));
                            System.Console.ResetColor();

                            System.Console.WriteLine();
                            System.Console.WriteLine(table);
                            System.Console.WriteLine();

                            System.Console.BackgroundColor = ConsoleColor.DarkCyan;
                            System.Console.ForegroundColor = ConsoleColor.White;
                            System.Console.WriteLine(CentrarTexto($"{lastUpdated.ToString("dd/MM/yyyy HH:mm:ss")}", System.Console.WindowWidth));
                            System.Console.ResetColor();

                        }
                    }
                    catch (Exception exInner)
                    {
                        System.Console.Clear();

                        System.Console.BackgroundColor = ConsoleColor.DarkGreen;
                        System.Console.ForegroundColor = ConsoleColor.White;
                        System.Console.WriteLine(CentrarTexto("MONITOREO DE KIOSKOS", System.Console.WindowWidth));
                        System.Console.ResetColor();

                        System.Console.WriteLine();

                        System.Console.BackgroundColor = ConsoleColor.DarkRed;
                        System.Console.ForegroundColor = ConsoleColor.White;
                        System.Console.WriteLine($"Error :{exInner.Message}");
                        System.Console.ResetColor();
                    }

                    Thread.Sleep(1000);
                }

            }
            catch (Exception ex)
            {
                System.Console.WriteLine($"No se puedo ejecutar el aplicativo por el error :{ex.Message}");
                System.Console.ReadLine();
            }
        }

        static string CentrarTexto(string texto, int longitud)
        {
            int spaces = longitud - texto.Length;
            int padLeft = spaces / 2 + texto.Length;

            return texto.PadLeft(padLeft).PadRight(longitud);
        }
    }
}
