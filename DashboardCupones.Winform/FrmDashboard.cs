﻿using MaterialSkin.Controls;
using MaterialSkin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using BE = DashboardCupones.Library.BusinessEntity;
using BL = DashboardCupones.Library.BusinessLogic;
using DN = DashboardCupones.Library.NetworkAccess;
using System.Drawing;

namespace DashboardCupones.Winform
{
    public partial class FrmDashboard : MaterialForm
    {

        private readonly MaterialSkinManager materialSkinManager;
        private int colorSchemeIndex;

        private Timer tmrActualizar = null;

        public FrmDashboard()
        {
            InitializeComponent();

            // Initialize MaterialSkinManager
            materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

            this.tmrActualizar = new Timer();
            this.tmrActualizar.Interval = 60 * 1000;
            this.tmrActualizar.Enabled = false;
            this.tmrActualizar.Tick += new EventHandler(this.tmrActualizar_tick);
            this.tmrActualizar.Start();
        }

        private void tmrActualizar_tick(object sender, EventArgs e)
        {
            try
            {
                if (btnActualizarKioskos.Enabled == true)
                {
                    this.btnActualizarKioskos.Enabled = false;

                    this.MonitoreoKioskos();

                    this.btnActualizarKioskos.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {
            try
            {

                #region Kioskos

                var lstBeKioskos = new List<BE.Kiosko>();
                var sorted = new SortableBindingList<BE.Kiosko>(lstBeKioskos);
                this.dgvKioskos.DataSource = sorted;
                this.lblKioskosActualizacion.Text = $"Ultima actualización {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";

                this.FormatKioskos();

                this.btnActualizarKioskos_Click(null, null);

                #endregion

                #region Experiencias

                var blKiosko = new BL.Kiosko();

                this.cboExperienciaEmpresas.DataSource = blKiosko.ListaSimpleEmpresas();
                this.cboExperienciaEmpresas.DisplayMember = "Nombre";
                this.cboExperienciaEmpresas.ValueMember = "Codigo";

                this.cboExperienciaTiendas.DataSource = blKiosko.ListaSimpleTiendas();
                this.cboExperienciaTiendas.DisplayMember = "Nombre";
                this.cboExperienciaTiendas.ValueMember = "Codigo";

                this.cboExperienciaHostnames.DataSource = blKiosko.ListaSimpleHostnames();
                this.cboExperienciaHostnames.DisplayMember = "Nombre";
                this.cboExperienciaHostnames.ValueMember = "Codigo";

                var lstBeExperiencias = new List<BE.Experiencia>();
                this.dgvExperiencias.DataSource = lstBeExperiencias;
                this.FormatExperiencias();

                this.CargarExperiencias();

                #endregion

                #region Clientes

                this.cboTopClientes.SelectedIndex = 0;

                this.CargarSesionesPorTiendas();
                this.CargarSesionesPorClientes();
                this.FormatTopClientes();

                var lstBeSesiones = new List<BE.Sesion>();
                var sortedSesiones = new SortableBindingList<BE.Sesion>(lstBeSesiones);
                this.dgvSesionesClientes.DataSource = sortedSesiones;
                this.FormatSesionessClientes();

                this.lblClientesActualizacion.Text = $"Ultima actualización {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";

                #endregion

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnActualizarExperiencias_Click(object sender, EventArgs e)
        {
            try
            {
                this.CargarExperiencias();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            colorSchemeIndex++;
            if (colorSchemeIndex > 2) colorSchemeIndex = 0;

            //These are just example color schemes
            switch (colorSchemeIndex)
            {
                case 0:
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
                    break;
                case 1:
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Indigo500, Primary.Indigo700, Primary.Indigo100, Accent.Pink200, TextShade.WHITE);
                    break;
                case 2:
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Green600, Primary.Green700, Primary.Green200, Accent.Red100, TextShade.WHITE);
                    break;
            }
        }

        private void btnTema_Click(object sender, EventArgs e)
        {
            materialSkinManager.Theme = materialSkinManager.Theme == MaterialSkinManager.Themes.DARK ? MaterialSkinManager.Themes.LIGHT : MaterialSkinManager.Themes.DARK;
        }

        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvKioskos.CurrentRow != null)
                {
                    var beKiosko = (BE.Kiosko)this.dgvKioskos.CurrentRow.DataBoundItem;
                    if (beKiosko != null && beKiosko.Hostname.Length > 0)
                    {
                        if (beKiosko.Conexion == "SI")
                        {
                            if (Util.ConfirmationMessage("¿Desea reiniciar el kiosko?") == false)
                                return;
                        }

                        string hostname = beKiosko.Hostname;

                        string rpta = DN.Kiosko.Restart(hostname);
                        if (rpta.ToUpper() == "RESTORING")
                        {
                            Util.InformationMessage("Kiosko reiniciandose");

                            beKiosko.Conexion = "NO";
                            beKiosko.Version = "";
                            beKiosko.Sesion = "";
                            beKiosko.Impreso = 0;
                            beKiosko.Restante = 0;

                            this.dgvKioskos.Refresh();

                            this.lblKioskoConectados.Text = (int.Parse(this.lblKioskoConectados.Text) - 1).ToString();
                            this.lblKioskoDesconectados.Text = (int.Parse(this.lblKioskoDesconectados.Text) + 1).ToString();
                        }
                        else if (rpta.ToUpper() == "BUSY")
                        {
                            if (Util.ConfirmationMessage("Kiosko ocupado ¿Desea reiniciar a la fuerza?") == true)
                            {
                                rpta = DN.Kiosko.Restart(hostname, true);
                                if (rpta.ToUpper() == "RESTORING BY FORCE")
                                {
                                    Util.InformationMessage("Kiosko reiniciandose a la fuerza");

                                    beKiosko.Conexion = "NO";
                                    beKiosko.Version = "";
                                    beKiosko.Sesion = "";
                                    beKiosko.Impreso = 0;
                                    beKiosko.Restante = 0;

                                    this.dgvKioskos.Refresh();

                                    this.lblKioskoConectados.Text = (int.Parse(this.lblKioskoConectados.Text) - 1).ToString();
                                    this.lblKioskoDesconectados.Text = (int.Parse(this.lblKioskoDesconectados.Text) + 1).ToString();
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnActualizarKioskos_Click(object sender, EventArgs e)
        {
            try
            {
                this.btnActualizarKioskos.Enabled = false;

                this.MonitoreoKioskos();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
            finally
            {
                this.btnActualizarKioskos.Enabled = true;
            }
        }
        
        private void btnActualizarClientes_Click(object sender, EventArgs e)
        {
            this.btnActualizarClientes.Enabled = false;
            try
            {

                this.cboTopClientes.SelectedIndex = 0;

                this.CargarSesionesPorTiendas();
                this.CargarSesionesPorClientes();

                var lstBeSesiones = new List<BE.Sesion>();
                var sortedSesiones = new SortableBindingList<BE.Sesion>(lstBeSesiones);
                this.dgvSesionesClientes.DataSource = sortedSesiones;

                this.lblClientesActualizacion.Text = $"Ultima actualización {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
            finally
            {
                this.btnActualizarClientes.Enabled = true;
            }
        }

        private void cboTopClientes_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                CargarSesionesPorClientes();

                this.lblClientesActualizacion.Text = $"Ultima actualización {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }


        private void cboExperienciaEmpresas_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {

                var blKiosko = new BL.Kiosko();

                if (this.cboExperienciaEmpresas.SelectedIndex > 0)
                {
                    var beEmpresa = (BE.General)this.cboExperienciaEmpresas.SelectedItem;
                    this.cboExperienciaTiendas.DataSource = blKiosko.ListaSimpleTiendas(beEmpresa.Codigo);
                }
                else
                {
                    this.cboExperienciaTiendas.DataSource = blKiosko.ListaSimpleTiendas();
                }
                this.cboExperienciaTiendas.DisplayMember = "Nombre";
                this.cboExperienciaTiendas.ValueMember = "Codigo";

                this.cboExperienciaHostnames.DataSource = blKiosko.ListaSimpleHostnames();
                this.cboExperienciaHostnames.DisplayMember = "Nombre";
                this.cboExperienciaHostnames.ValueMember = "Codigo";

                this.CargarExperiencias();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void cboExperienciaTiendas_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {

                var blKiosko = new BL.Kiosko();

                if (this.cboExperienciaTiendas.SelectedIndex > 0)
                {
                    var beTienda = (BE.General)this.cboExperienciaTiendas.SelectedItem;
                    this.cboExperienciaHostnames.DataSource = blKiosko.ListaSimpleHostnames(beTienda.Codigo);
                }
                else
                {
                    this.cboExperienciaHostnames.DataSource = blKiosko.ListaSimpleHostnames();
                }
                this.cboExperienciaHostnames.DisplayMember = "Nombre";
                this.cboExperienciaHostnames.ValueMember = "Codigo";

                this.CargarExperiencias();

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void cboExperienciaHostnames_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                this.CargarExperiencias();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void MonitoreoKioskos()
        {
            try
            {
                var bl = new BL.Kiosko();

                #region Ultima fecha de Monitoreo

                DateTime fechaHora = bl.UltimoMonitoreo();
                this.lblKioskosActualizacion.Text = $"Ultima actualización {fechaHora.ToString("dd/MM/yyyy HH:mm:ss")}";

                #endregion

                #region Listado de ultimo Monitoreo

                List<BE.Kiosko> lstBeKioskos = bl.Monitoreo(fechaHora);
                this.dgvKioskos.DataSource = new SortableBindingList<BE.Kiosko>(lstBeKioskos);

                #endregion

                Task.Run(() => bl.Registrar());

                #region Grafico de Monitoreo de kioskos Conectados y Desconectados
                if (lstBeKioskos.Count > 0)
                {

                    var lstBeKioskosConexion = lstBeKioskos
                                                .GroupBy(a => a.Conexion)
                                                .Select(a => new
                                                {
                                                    Conexion = a.Key,
                                                    Total = a.Count()
                                                });

                    string[] x = (from p in lstBeKioskosConexion
                                  orderby p.Conexion ascending
                                  select p.Conexion).ToArray();

                    int[] y = (from p in lstBeKioskosConexion
                               orderby p.Conexion ascending
                               select p.Total).ToArray();

                    this.chrKioskos.Series[0].ChartType = SeriesChartType.Doughnut;
                    this.chrKioskos.Series[0].Points.DataBindXY(x, y);
                    this.chrKioskos.Series[0].IsValueShownAsLabel = true;
                    this.chrKioskos.Series[0].Label = "#PERCENT{P2}";
                    this.chrKioskos.ChartAreas[0].Area3DStyle.Enable3D = true;

                    this.lblKioskoConectados.Text = lstBeKioskosConexion
                                                    .FirstOrDefault(g => g.Conexion.ToUpper().Equals("SI"))
                                                    .Total
                                                    .ToString();

                    this.lblKioskoDesconectados.Text = lstBeKioskosConexion
                                                    .FirstOrDefault(g => g.Conexion.ToUpper().Equals("NO"))
                                                    .Total
                                                    .ToString();
                }
                else
                {
                    this.lblKioskoConectados.Text = "0";
                    this.lblKioskoDesconectados.Text = "0";
                }

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatKioskos()
        {
            try
            {
                Util.FormatDatagridview(ref dgvKioskos);

                this.dgvKioskos.Columns["Empresa"].Visible = true;
                this.dgvKioskos.Columns["Empresa"].HeaderText = "Empresa";
                this.dgvKioskos.Columns["Empresa"].Width = 80;
                this.dgvKioskos.Columns["Empresa"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvKioskos.Columns["Tienda"].Visible = true;
                this.dgvKioskos.Columns["Tienda"].HeaderText = "Tienda";
                this.dgvKioskos.Columns["Tienda"].Width = 80;
                this.dgvKioskos.Columns["Tienda"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvKioskos.Columns["Hostname"].Visible = true;
                this.dgvKioskos.Columns["Hostname"].HeaderText = "Hostname";
                this.dgvKioskos.Columns["Hostname"].Width = 80;
                this.dgvKioskos.Columns["Hostname"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvKioskos.Columns["Ip"].Visible = false;

                this.dgvKioskos.Columns["Conexion"].Visible = true;
                this.dgvKioskos.Columns["Conexion"].HeaderText = "Conexion";
                this.dgvKioskos.Columns["Conexion"].Width = 80;
                this.dgvKioskos.Columns["Conexion"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Version"].Visible = true;
                this.dgvKioskos.Columns["Version"].HeaderText = "Version";
                this.dgvKioskos.Columns["Version"].Width = 80;
                this.dgvKioskos.Columns["Version"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Sesion"].Visible = true;
                this.dgvKioskos.Columns["Sesion"].HeaderText = "Sesion";
                this.dgvKioskos.Columns["Sesion"].Width = 80;
                this.dgvKioskos.Columns["Sesion"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvKioskos.Columns["Impreso"].Visible = true;
                this.dgvKioskos.Columns["Impreso"].HeaderText = "Impreso";
                this.dgvKioskos.Columns["Impreso"].Width = 80;
                this.dgvKioskos.Columns["Impreso"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.dgvKioskos.Columns["Restante"].Visible = true;
                this.dgvKioskos.Columns["Restante"].HeaderText = "Restante";
                this.dgvKioskos.Columns["Restante"].Width = 80;
                this.dgvKioskos.Columns["Restante"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                Util.AutoWidthColumn(ref dgvKioskos, "Hostname");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatExperiencias()
        {
            try
            {
                Util.FormatDatagridview(ref dgvExperiencias);

                this.dgvExperiencias.Columns["Id"].Visible = false;

                this.dgvExperiencias.Columns["Nombre"].Visible = true;
                this.dgvExperiencias.Columns["Nombre"].HeaderText = "Experiencia";
                this.dgvExperiencias.Columns["Nombre"].Width = 80;
                this.dgvExperiencias.Columns["Nombre"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvExperiencias.Columns["Cantidad"].Visible = true;
                this.dgvExperiencias.Columns["Cantidad"].HeaderText = "Cantidad";
                this.dgvExperiencias.Columns["Cantidad"].Width = 80;
                this.dgvExperiencias.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.dgvExperiencias.Columns["Porcentaje"].Visible = true;
                this.dgvExperiencias.Columns["Porcentaje"].HeaderText = "Porcentaje";
                this.dgvExperiencias.Columns["Porcentaje"].Width = 80;
                this.dgvExperiencias.Columns["Porcentaje"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                this.dgvExperiencias.Columns["Porcentaje"].DefaultCellStyle.Format = "N2";

                Util.AutoWidthColumn(ref dgvExperiencias, "Nombre", false);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatTopClientes()
        {
            try
            {
                Util.FormatDatagridview(ref dgvTopClientes);

                this.dgvTopClientes.Columns["TipoDocumento"].Visible = true;
                this.dgvTopClientes.Columns["TipoDocumento"].HeaderText = "Documento";
                this.dgvTopClientes.Columns["TipoDocumento"].Width = 80;
                this.dgvTopClientes.Columns["TipoDocumento"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvTopClientes.Columns["NroDocumento"].Visible = true;
                this.dgvTopClientes.Columns["NroDocumento"].HeaderText = "Numero";
                this.dgvTopClientes.Columns["NroDocumento"].Width = 80;
                this.dgvTopClientes.Columns["NroDocumento"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                
                this.dgvTopClientes.Columns["Cantidad"].Visible = true;
                this.dgvTopClientes.Columns["Cantidad"].HeaderText = "Sesiones";
                this.dgvTopClientes.Columns["Cantidad"].Width = 80;
                this.dgvTopClientes.Columns["Cantidad"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                
                Util.AutoWidthColumn(ref dgvTopClientes, "NroDocumento");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatSesionessClientes()
        {
            try
            {
                Util.FormatDatagridview(ref dgvSesionesClientes);

                this.dgvSesionesClientes.Columns["Tienda"].Visible = true;
                this.dgvSesionesClientes.Columns["Tienda"].HeaderText = "Tienda";
                this.dgvSesionesClientes.Columns["Tienda"].Width = 80;
                this.dgvSesionesClientes.Columns["Tienda"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvSesionesClientes.Columns["Fecha"].Visible = true;
                this.dgvSesionesClientes.Columns["Fecha"].HeaderText = "Fecha";
                this.dgvSesionesClientes.Columns["Fecha"].Width = 80;
                this.dgvSesionesClientes.Columns["Fecha"].DefaultCellStyle.Format = "dd/MM/yyyy";
                this.dgvSesionesClientes.Columns["Fecha"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvSesionesClientes.Columns["Inicio"].Visible = true;
                this.dgvSesionesClientes.Columns["Inicio"].HeaderText = "Inicio";
                this.dgvSesionesClientes.Columns["Inicio"].Width = 80;
                this.dgvSesionesClientes.Columns["Inicio"].DefaultCellStyle.Format = "HH:mm:ss";
                this.dgvSesionesClientes.Columns["Inicio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvSesionesClientes.Columns["Fin"].Visible = true;
                this.dgvSesionesClientes.Columns["Fin"].HeaderText = "Fin";
                this.dgvSesionesClientes.Columns["Fin"].Width = 80;
                this.dgvSesionesClientes.Columns["Fin"].DefaultCellStyle.Format = "HH:mm:ss";
                this.dgvSesionesClientes.Columns["Fin"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                this.dgvSesionesClientes.Columns["Duracion"].Visible = true;
                this.dgvSesionesClientes.Columns["Duracion"].HeaderText = "Duracion";
                this.dgvSesionesClientes.Columns["Duracion"].Width = 80;
                this.dgvSesionesClientes.Columns["Duracion"].DefaultCellStyle.Format = "HH:mm:ss";
                this.dgvSesionesClientes.Columns["Duracion"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

                Util.AutoWidthColumn(ref dgvSesionesClientes, "Tienda");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CargarExperiencias()
        {
            try
            {
                string empresa = "";
                string tienda = "";
                string hostname = "";

                var beEmpresa = (BE.General)this.cboExperienciaEmpresas.SelectedItem;
                if (beEmpresa != null && beEmpresa.Codigo.Length > 0)
                    empresa = beEmpresa.Codigo;

                var beTienda = (BE.General)this.cboExperienciaTiendas.SelectedItem;
                if (beTienda != null && beTienda.Codigo.Length > 0)
                    tienda = beTienda.Codigo;

                var beHostname = (BE.General)this.cboExperienciaHostnames.SelectedItem;
                if (beHostname != null && beHostname.Codigo.Length > 0)
                    hostname = beHostname.Codigo;

                var lstExperiencias = new BL.Experiencia().Resumen(empresa, tienda, hostname);

                string[] x = (from p in lstExperiencias
                               orderby p.Nombre ascending
                               select p.Nombre).ToArray();

                int[] y = (from p in lstExperiencias
                           orderby p.Nombre ascending
                           select p.Cantidad).ToArray();

                chtExperienciasResumen.Series[0].ChartType = SeriesChartType.Pie;
                chtExperienciasResumen.Series[0].Points.DataBindXY(x, y);
                chtExperienciasResumen.Series[0].IsValueShownAsLabel = true;
                chtExperienciasResumen.Series[0].Label = "#PERCENT{P2}";
                chtExperienciasResumen.ChartAreas[0].Area3DStyle.Enable3D = true;

                chtExperienciasDetalle.Series[0].ChartType = SeriesChartType.Bar;
                chtExperienciasDetalle.Series[0].Points.DataBindXY(x, y);
                chtExperienciasDetalle.Series[0].IsValueShownAsLabel = true;
                chtExperienciasDetalle.Series[0].Label = "#VALY";
                chtExperienciasDetalle.Series[0].CustomProperties = "BarLabelStyle = Right";
                chtExperienciasDetalle.ChartAreas[0].Area3DStyle.Enable3D = false;

                this.dgvExperiencias.DataSource = lstExperiencias;

                this.lblTotCantExperiencias.Text = lstExperiencias.Sum(o => o.Cantidad).ToString();
                this.lblTotPrcExperiencias.Text = lstExperiencias.Sum(o => o.Porcentaje).ToString("N2");

                this.lblExperienciasActualizacion.Text = $"Ultima actualización {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }


        private void CargarSesionesPorTiendas()
        {
            try
            {
                var lstSesiones = new BL.Sesion().PorTiendas();

                string[] x = (from p in lstSesiones
                              orderby p.Tienda ascending
                              select p.Tienda).ToArray();

                int[] y = (from p in lstSesiones
                           orderby p.Tienda ascending
                           select p.Cantidad).ToArray();

                this.chtSesionesPorTiendas.Series[0].ChartType = SeriesChartType.Bar;
                this.chtSesionesPorTiendas.Series[0].Points.DataBindXY(x, y);
                this.chtSesionesPorTiendas.Series[0].IsValueShownAsLabel = true;
                this.chtSesionesPorTiendas.Series[0].Label = "TIENDA : #VALX - CLIENTES : #VALY";

                this.chtSesionesPorTiendas.Series[0].Sort(PointSortOrder.Ascending);

                this.chtSesionesPorTiendas.ChartAreas[0].Area3DStyle.Enable3D = false;
                this.chtSesionesPorTiendas.ChartAreas[0].AxisX.Enabled = AxisEnabled.False;
                this.chtSesionesPorTiendas.ChartAreas[0].AxisY.Enabled = AxisEnabled.False;

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void CargarSesionesPorClientes()
        {
            try
            {
                int limite = int.Parse(this.cboTopClientes.SelectedItem.ToString());
                var lstBeSesionesClientes = new BL.Sesion().TopClientes(limite);

                var sortedSesionesClientes = new SortableBindingList<BE.SesionCliente>(lstBeSesionesClientes);
                this.dgvTopClientes.DataSource = sortedSesionesClientes;

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void CargarSesionesPorCliente(string nroDocumento)
        {
            try
            {
                var lstBeSesiones = new BL.Sesion().Destalle(nroDocumento);

                var sortedSesiones = new SortableBindingList<BE.Sesion>(lstBeSesiones);
                this.dgvSesionesClientes.DataSource = sortedSesiones;

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dgvTopClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.dgvTopClientes.CurrentRow != null)
                {
                    var beSesionCliente = (BE.SesionCliente)this.dgvTopClientes.CurrentRow.DataBoundItem;

                    this.CargarSesionesPorCliente(beSesionCliente.NroDocumento);
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dgvKioskos_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow Myrow in dgvKioskos.Rows)
                {
                    if (Myrow.Cells["Conexion"].Value.Equals("SI"))
                    {
                        Myrow.Cells["Conexion"].Style.ForeColor = Color.White;
                        Myrow.Cells["Conexion"].Style.BackColor = Color.Green;
                    }
                    else
                    {
                        Myrow.Cells["Conexion"].Style.ForeColor = Color.White;
                        Myrow.Cells["Conexion"].Style.BackColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnCuponMiniatura_Click(object sender, EventArgs e)
        {
            try
            {
                var min = FrmSuperCoupon.Instance();
                min.Show();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnCuponDiseno_Click(object sender, EventArgs e)
        {
            try
            {
                var detail = FrmCouponView.Instance();
                detail.Show();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnCuponSuper_Click(object sender, EventArgs e)
        {
            try
            {
                var super = FrmCouponSuper.Instance();
                super.Show();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }
    }

}
