﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using BL = DashboardCupones.Library.BusinessLogic;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Winform
{
    public partial class FrmSuperCoupon : MaterialForm
    {

        #region "Singletons"

        private static FrmSuperCoupon frmInstance = null;

        public static FrmSuperCoupon Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmSuperCoupon();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        public FrmSuperCoupon()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                MessageBox.Show(args.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                MessageBox.Show((args.ExceptionObject as Exception).Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void CouponList_Load(object sender, EventArgs e)
        {
            try
            {
                #region Ambito

                DataTable dtAmbito = new DataTable();
                dtAmbito.Columns.Add("codigo", typeof(string));
                dtAmbito.Columns.Add("nombre", typeof(string));

                DataRow drAmbito = null;
                drAmbito = dtAmbito.NewRow();
                drAmbito["codigo"] = "WONG";
                drAmbito["nombre"] = "Wong";
                dtAmbito.Rows.Add(drAmbito);
                drAmbito = dtAmbito.NewRow();
                drAmbito["codigo"] = "METRO";
                drAmbito["nombre"] = "Metro";
                dtAmbito.Rows.Add(drAmbito);

                this.CargarComboBox(ref this.cboAmbito, dtAmbito);

                #endregion

                #region Grupo Cupon

                DataTable dtGrupo = new DataTable();
                dtGrupo.Columns.Add("codigo", typeof(int));
                dtGrupo.Columns.Add("nombre", typeof(string));

                DataRow drGrupo = null;
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 1;
                drGrupo["nombre"] = "Disponible";
                dtGrupo.Rows.Add(drGrupo);
                //drGrupo = dtGrupo.NewRow();
                //drGrupo["codigo"] = 2;
                //drGrupo["nombre"] = "Super";
                //Grupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 3;
                drGrupo["nombre"] = "Marca";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 4;
                drGrupo["nombre"] = "Lugar";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 5;
                drGrupo["nombre"] = "Ruleta";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 6;
                drGrupo["nombre"] = "Arbol";
                dtGrupo.Rows.Add(drGrupo);
                //drGrupo = dtGrupo.NewRow();
                //drGrupo["codigo"] = 7;
                //drGrupo["nombre"] = "Bonus";
                //dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 8;
                drGrupo["nombre"] = "Invitacion";
                dtGrupo.Rows.Add(drGrupo);

                this.CargarComboBox(ref this.cboGrupoCupon, dtGrupo);

                #endregion

                #region Tipo de Documento

                DataTable dtTipoDocumento = new DataTable();
                dtTipoDocumento.Columns.Add("codigo", typeof(string));
                dtTipoDocumento.Columns.Add("nombre", typeof(string));

                DataRow drTipoDocumento = null;
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "D";
                drTipoDocumento["nombre"] = "DNI";
                dtTipoDocumento.Rows.Add(drTipoDocumento);
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "B";
                drTipoDocumento["nombre"] = "BONUS";
                dtTipoDocumento.Rows.Add(drTipoDocumento);

                this.CargarComboBox(ref this.cboTipoDocumento, dtTipoDocumento);

                #endregion

                var lstCoupons = new List<BE.Cupon>();
                var sorted = new SortableBindingList<BE.Cupon>(lstCoupons);
                this.dgvCoupons.DataSource = sorted;

                this.FormatCoupons();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtNroDocumento.Text.Trim().Length == 0)
                {
                    this.txtNroDocumento.Focus();
                    MessageBox.Show("Ingrese el número de documento", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string nroDocumento = this.txtNroDocumento.Text.Trim();

                string ambito = this.cboAmbito.SelectedValue.ToString();
                string tienda = this.txtTienda.Text.Trim();
                int idGrupoCupon = int.Parse(this.cboGrupoCupon.SelectedValue.ToString());
                int idTipoCupon = 0;

                this.MostrarCupones(nroDocumento, idGrupoCupon, ambito, tienda, idTipoCupon);
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void txtNroDocumento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNroDocumento.Text.Trim().Length > 8)
                {
                    this.cboTipoDocumento.SelectedValue = "B";
                }
                else
                {
                    this.cboTipoDocumento.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void dgvCoupons_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.dgvCoupons.CurrentRow != null)
                {
                    var coupon = (BE.Cupon)this.dgvCoupons.CurrentRow.DataBoundItem;

                    this.LoadCouponImagen(coupon);
                    this.LoadCouponDetail(coupon);
                }
                else
                {
                    this.LoadCouponImagen(null);
                    this.LoadCouponDetail(null);
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.dgvCoupons.CurrentRow != null)
                {
                    var coupon = (BE.Cupon)this.dgvCoupons.CurrentRow.DataBoundItem;

                    coupon.Descuento = this.txtDescuento.Text;
                    coupon.Mensaje1 = this.txtMensaje01.Text;
                    coupon.Mensaje2 = this.txtMensaje02.Text;
                    coupon.Mensaje3 = this.txtMensaje03.Text;
                    coupon.UrlImagen = this.txtUrlImagen.Text;

                    this.LoadCouponImagen(coupon);
                    this.LoadCouponDetail(coupon);
                }
                else
                {
                    this.LoadCouponImagen(null);
                    this.LoadCouponDetail(null);
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void CargarComboBox(ref ComboBox cbo, DataTable dt)
        {
            cbo.DataSource = dt;
            cbo.DisplayMember = dt.Columns[1].Caption;
            cbo.ValueMember = dt.Columns[0].Caption;
        }

        public Image CargarImagen(string url)
        {
            Image imagen = null;
            try
            {

                Uri uri = new Uri(url);
                string filename = Path.GetFileName(uri.LocalPath);

                string directory = "Cache";

                string path = Path.Combine(directory, filename);

                if (File.Exists(path) == true)
                {
                    imagen = Image.FromFile(path);
                }
                else
                {
                    if (Directory.Exists(directory) == false)
                        Directory.CreateDirectory(directory);

                    using (var client = new WebClient())
                    {
                        client.DownloadFile(url, path);
                    }
                    imagen = Image.FromFile(path);
                }


                return imagen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SerialBonus(string numero)
        {
            try
            {
                string bonusSeriado = "";

                string numeroBonus = numero.Trim();
                if (numeroBonus.Length == 19)
                {
                    int cntVisibleDerecha = 3;
                    int cntVisibleIzquierda = 3;
                    int cntSeriado = numeroBonus.Length - cntVisibleDerecha - cntVisibleIzquierda;

                    bonusSeriado = numeroBonus.Substring(0, cntVisibleDerecha)
                                + new String('*', cntSeriado)
                                + numeroBonus.Substring(numeroBonus.Length - cntVisibleIzquierda);
                }

                return bonusSeriado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void MostrarCupones(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            try
            {
                var bl = new BL.Cupon(nroDocumento, idGrupo, tipoCupon);
                var lstCoupons = bl.Listar(ambito,tienda);
                if (lstCoupons != null)
                {
                    var sorted = new SortableBindingList<BE.Cupon>(lstCoupons);
                    this.dgvCoupons.DataSource = sorted;
                }       
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FormatCoupons()
        {
            try
            {
                Util.FormatDatagridview(ref dgvCoupons);

                for (int i = 0; i < this.dgvCoupons.Columns.Count; i++)
                {
                    this.dgvCoupons.Columns[i].Visible = false;
                }
                this.dgvCoupons.Columns["Id"].Visible = true;
                this.dgvCoupons.Columns["Id"].HeaderText = "Cupon";
                this.dgvCoupons.Columns["Id"].Width = 80;
                this.dgvCoupons.Columns["Id"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["Descuento"].Visible = true;
                this.dgvCoupons.Columns["Descuento"].HeaderText = "Descuento";
                this.dgvCoupons.Columns["Descuento"].Width = 80;
                this.dgvCoupons.Columns["Descuento"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                this.dgvCoupons.Columns["Mensaje1"].Visible = true;
                this.dgvCoupons.Columns["Mensaje1"].HeaderText = "Mensaje 1";
                this.dgvCoupons.Columns["Mensaje1"].Width = 120;
                this.dgvCoupons.Columns["Mensaje1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["Mensaje2"].Visible = true;
                this.dgvCoupons.Columns["Mensaje2"].HeaderText = "Mensaje 2";
                this.dgvCoupons.Columns["Mensaje2"].Width = 120;
                this.dgvCoupons.Columns["Mensaje2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["Mensaje3"].Visible = true;
                this.dgvCoupons.Columns["Mensaje3"].HeaderText = "Mensaje 3";
                this.dgvCoupons.Columns["Mensaje3"].Width = 120;
                this.dgvCoupons.Columns["Mensaje3"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                this.dgvCoupons.Columns["UrlImagen"].Visible = true;
                this.dgvCoupons.Columns["UrlImagen"].HeaderText = "Url Imagen";
                this.dgvCoupons.Columns["UrlImagen"].Width = 200;
                this.dgvCoupons.Columns["UrlImagen"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

                Util.AutoWidthColumn(ref dgvCoupons, "UrlImagen");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadCouponImagen(BE.Cupon coupon = null)
        {
            try
            {
                Image miniatura = null;

                if (coupon != null)
                {
                    string urlImagen = coupon.UrlImagen.Trim();
                    if (urlImagen == null || urlImagen.Length == 0)
                        urlImagen = "https://cencosud.vridge.io:83/kiosko2/kiosko_default.jpg";

                    // Obtener imagen 
                    Image imagen = this.CargarImagen(urlImagen);

                    // Ajustar tamaño de la imagen
                    miniatura = (Image)(new Bitmap(imagen, new Size(355, 163)));

                    string firstText = coupon.Descuento;
                    string secondText = coupon.Mensaje1;
                    string thirdText = coupon.Mensaje2;
                    string fourthText = coupon.Mensaje3;

                    // Set format of string.
                    StringFormat drawFormat = new StringFormat();
                    drawFormat.Alignment = StringAlignment.Center;

                    using (Graphics graphics = Graphics.FromImage(miniatura))
                    {
                        using (Font arialFont = new Font("Arial Black", 25))
                        {
                            graphics.DrawString(firstText, arialFont, Brushes.Red, 230f, 30f, drawFormat);
                        }

                        using (Font arialFont = new Font("Arial Black", 13))
                        {
                            graphics.DrawString(secondText, arialFont, Brushes.Black, 230f, 70f, drawFormat);
                            graphics.DrawString(thirdText, arialFont, Brushes.Black, 230f, 90f, drawFormat);
                            graphics.DrawString(fourthText, arialFont, Brushes.Black, 230f, 110f, drawFormat);
                        }
                    }
                }

                this.picCoupon.Image =  miniatura;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadCouponDetail(BE.Cupon coupon = null)
        {
            try
            {
                if (coupon != null)
                {
                    this.txtCupon.Text = coupon.Id.ToString();
                    this.txtDescuento.Text = coupon.Descuento;
                    this.txtMensaje01.Text = coupon.Mensaje1;
                    this.txtMensaje02.Text = coupon.Mensaje2;
                    this.txtMensaje03.Text = coupon.Mensaje3;
                    this.txtUrlImagen.Text = coupon.UrlImagen;
                }
                else
                {
                    this.txtCupon.Clear();
                    this.txtDescuento.Clear();
                    this.txtMensaje01.Clear();
                    this.txtMensaje02.Clear();
                    this.txtMensaje03.Clear();
                    this.txtUrlImagen.Clear();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
