﻿using MaterialSkin.Controls;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BL = DashboardCupones.Library.BusinessLogic;
using BE = DashboardCupones.Library.BusinessEntity;
using System.Drawing.Imaging;

namespace DashboardCupones.Winform
{
    public partial class FrmCouponView : MaterialForm
    {

        #region "Singletons"

        private static FrmCouponView frmInstance = null;

        public static FrmCouponView Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmCouponView();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        private string nroDocumento = "";
        private int nroCupon = 0;
        private int idGrupoCupon = 0;
        private int idTipoCupon = 0;

        private BE.Cupon beCoupon = null;

        public FrmCouponView()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                MessageBox.Show(args.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                MessageBox.Show((args.ExceptionObject as Exception).Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void Cupoun_Load(object sender, EventArgs e)
        {
            try
            {
                #region Ambito

                DataTable dtAmbito = new DataTable();
                dtAmbito.Columns.Add("codigo", typeof(string));
                dtAmbito.Columns.Add("nombre", typeof(string));

                DataRow drAmbito = null;
                drAmbito = dtAmbito.NewRow();
                drAmbito["codigo"] = "WONG";
                drAmbito["nombre"] = "Wong";
                dtAmbito.Rows.Add(drAmbito);
                drAmbito = dtAmbito.NewRow();
                drAmbito["codigo"] = "METRO";
                drAmbito["nombre"] = "Metro";
                dtAmbito.Rows.Add(drAmbito);

                this.CargarComboBox(ref this.cboAmbito, dtAmbito);

                #endregion

                #region Grupo Cupon

                DataTable dtGrupo = new DataTable();
                dtGrupo.Columns.Add("codigo", typeof(int));
                dtGrupo.Columns.Add("nombre", typeof(string));

                DataRow drGrupo = null;
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 1;
                drGrupo["nombre"] = "Disponible";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 2;
                drGrupo["nombre"] = "Super";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 3;
                drGrupo["nombre"] = "Marca";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 4;
                drGrupo["nombre"] = "Lugar";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 5;
                drGrupo["nombre"] = "Ruleta";
                dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 6;
                drGrupo["nombre"] = "Arbol";
                dtGrupo.Rows.Add(drGrupo);
                //drGrupo = dtGrupo.NewRow();
                //drGrupo["codigo"] = 7;
                //drGrupo["nombre"] = "Bonus";
                //dtGrupo.Rows.Add(drGrupo);
                drGrupo = dtGrupo.NewRow();
                drGrupo["codigo"] = 8;
                drGrupo["nombre"] = "Invitacion";
                dtGrupo.Rows.Add(drGrupo);

                this.CargarComboBox(ref this.cboGrupoCupon, dtGrupo);

                #endregion

                #region Tipo Cupon

                DataTable dtTipo = new DataTable();
                dtTipo.Columns.Add("codigo", typeof(int));
                dtTipo.Columns.Add("nombre", typeof(string));

                DataRow drTipo = null;
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 2;
                drTipo["nombre"] = "Normal";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 1;
                drTipo["nombre"] = "Regalo";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 3;
                drTipo["nombre"] = "Horario";
                dtTipo.Rows.Add(drTipo);

                this.CargarComboBox(ref this.cboTipoCupon, dtTipo);

                this.cboTipoCupon.Enabled = false;

                #endregion

                #region Tipo Legal

                DataTable dtTipoLegal = new DataTable();
                dtTipoLegal.Columns.Add("codigo", typeof(int));
                dtTipoLegal.Columns.Add("nombre", typeof(string));

                DataRow drTipoLegal = null;
                drTipoLegal = dtTipoLegal.NewRow();
                drTipoLegal["codigo"] = 0;
                drTipoLegal["nombre"] = "Normal";
                dtTipoLegal.Rows.Add(drTipoLegal);
                drTipoLegal = dtTipoLegal.NewRow();
                drTipoLegal["codigo"] = 1;
                drTipoLegal["nombre"] = "Bebida Alcoholicas";
                dtTipoLegal.Rows.Add(drTipoLegal);

                this.CargarComboBox(ref this.cboTipoLegal, dtTipoLegal);

                #endregion

                #region Tipo de Documento

                DataTable dtTipoDocumento = new DataTable();
                dtTipoDocumento.Columns.Add("codigo", typeof(string));
                dtTipoDocumento.Columns.Add("nombre", typeof(string));

                DataRow drTipoDocumento = null;
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "D";
                drTipoDocumento["nombre"] = "DNI";
                dtTipoDocumento.Rows.Add(drTipoDocumento);
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "B";
                drTipoDocumento["nombre"] = "BONUS";
                dtTipoDocumento.Rows.Add(drTipoDocumento);

                this.CargarComboBox(ref this.cboTipoDocumento, dtTipoDocumento);

                #endregion

                #region Canal

                DataTable dtCanal = new DataTable();
                dtCanal.Columns.Add("codigo", typeof(int));
                dtCanal.Columns.Add("nombre", typeof(string));

                DataRow drCanal = null;
                drCanal = dtCanal.NewRow();
                drCanal["codigo"] = 1;
                drCanal["nombre"] = "Tienda";
                dtCanal.Rows.Add(drCanal);
                drCanal = dtCanal.NewRow();
                drCanal["codigo"] = 2;
                drCanal["nombre"] = "Online";
                dtCanal.Rows.Add(drCanal);
                drCanal = dtCanal.NewRow();
                drCanal["codigo"] = 3;
                drCanal["nombre"] = "Tienda y Online";
                dtCanal.Rows.Add(drCanal);

                this.CargarComboBox(ref this.cboCanal, dtCanal);

                #endregion
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                this.picCoupon.Image = this.ObtenerImagen();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.beCoupon == null)
                    return;

                if (this.picCoupon == null)
                    return;

                var dialog = new SaveFileDialog();
                dialog.Filter = "Imagen JPG |*.jpg|Imagen PNG|*.png";
                dialog.Title = "Exportar imagen del cupon";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.picCoupon.Image.Save(dialog.FileName, ImageFormat.Png);
                }

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtNroDocumento.Text.Trim().Length == 0)
                {
                    this.txtNroDocumento.Focus();
                    MessageBox.Show("Ingrese el número de documento", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (this.txtNroCupon.Text.Trim().Length == 0)
                {
                    this.txtNroCupon.Focus();
                    MessageBox.Show("Ingrese el número de cupon", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                this.nroDocumento = this.txtNroDocumento.Text.Trim();
                this.nroCupon = int.Parse(this.txtNroCupon.Text.Trim());
                this.idGrupoCupon = int.Parse(this.cboGrupoCupon.SelectedValue.ToString());
                this.idTipoCupon = int.Parse(this.cboTipoCupon.SelectedValue.ToString());

                this.ObtenerCupon(nroDocumento, nroCupon, idGrupoCupon, idTipoCupon);

                this.picCoupon.Image = this.ObtenerImagen();
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void cboGrupoCupon_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                if (this.cboGrupoCupon.SelectedValue.ToString() != "2")
                {
                    this.cboTipoCupon.Enabled = false;
                    this.cboTipoCupon.SelectedValue = "2";
                }
                else
                {
                    this.cboTipoCupon.Enabled = true;
                    this.cboTipoCupon.SelectedValue = "2";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void txtNroDocumento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNroDocumento.Text.Trim().Length > 8)
                {
                    this.cboTipoDocumento.SelectedValue = "B";
                }
                else
                {
                    this.cboTipoDocumento.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void picCoupon_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.picCoupon.Image != null)
                {
                   
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void CargarComboBox(ref ComboBox cbo, DataTable dt)
        {
            cbo.DataSource = dt;
            cbo.DisplayMember = dt.Columns[1].Caption;
            cbo.ValueMember = dt.Columns[0].Caption;
        }

        public Image CargarImagen(string url)
        {
            Image imagen = null;
            try
            {

                Uri uri = new Uri(url);
                string filename = Path.GetFileName(uri.LocalPath);

                string directory = "Cache";

                string path = Path.Combine(directory, filename);

                if (File.Exists(path) == true)
                {
                    imagen = Image.FromFile(path);
                }
                else
                {
                    if (Directory.Exists(directory) == false)
                        Directory.CreateDirectory(directory);

                    using (var client = new WebClient())
                    {
                        client.DownloadFile(url, path);
                    }
                    imagen = Image.FromFile(path);
                }


                return imagen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SerialBonus(string numero)
        {
            try
            {
                string bonusSeriado = "";

                string numeroBonus = numero.Trim();
                if (numeroBonus.Length == 19)
                {
                    int cntVisibleDerecha = 3;
                    int cntVisibleIzquierda = 3;
                    int cntSeriado = numeroBonus.Length - cntVisibleDerecha - cntVisibleIzquierda;

                    bonusSeriado = numeroBonus.Substring(0, cntVisibleDerecha)
                                + new String('*', cntSeriado)
                                + numeroBonus.Substring(numeroBonus.Length - cntVisibleIzquierda);
                }

                return bonusSeriado;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private Image ObtenerImagen()
        {
            Image impresionCupon = null;
            try
            {

                #region Imagen del Cupon
                var logoCupon = this.CargarImagen(this.txtUrlImagen.Text.Trim());
                #endregion

                #region Parrafo de Aviso Legal
                int legalLen = 40;
                string legal = this.txtMensajeLegal.Text.Trim();
                string[] linelegal = Regex.Matches(legal, ".{1," + legalLen + "}")
                                    .Cast<Match>()
                                    .Select(m => m.Value)
                                    .ToArray();
                #endregion

                #region Parrafo de Descuento del Cupon
                int descuentoLen = 7;
                string descuento = this.txtDescuento.Text.Trim();
                descuento = (descuento.Length > descuentoLen ? descuento.Substring(1, descuentoLen) : descuento);
                #endregion

                #region Parrafo de Descripcion del Cupon
                int mensajeLen = 22;
                string mensaje01 = this.txtMensaje01.Text.Trim();
                mensaje01 = (mensaje01.Length > mensajeLen ? mensaje01.Substring(1, mensajeLen) : mensaje01);
                string mensaje02 = this.txtMensaje02.Text.Trim();
                mensaje02 = (mensaje02.Length > mensajeLen ? mensaje02.Substring(1, mensajeLen) : mensaje02);
                string mensaje03 = this.txtMensaje03.Text.Trim();
                mensaje03 = (mensaje03.Length > mensajeLen ? mensaje03.Substring(1, mensajeLen) : mensaje03);
                #endregion

                #region Imagen del Fondo de Ticket
                Image imagenCupon = null;
                if (this.cboAmbito.SelectedValue.ToString() == "WONG")
                {
                    if (this.cboTipoLegal.SelectedValue.ToString() == "1") //Bebida Alcoholicas
                        imagenCupon = global::DashboardCupones.Winform.Properties.Resources.cupon_bebida_wong;
                    else //Normal
                        imagenCupon = global::DashboardCupones.Winform.Properties.Resources.cupon_normal_wong;
                }
                else if (this.cboAmbito.SelectedValue.ToString() == "METRO")
                {
                    if (this.cboTipoLegal.SelectedValue.ToString() == "1") //Bebida Alcoholicas
                        imagenCupon = global::DashboardCupones.Winform.Properties.Resources.cupon_bebida_metro;
                    else //Normal
                        imagenCupon = global::DashboardCupones.Winform.Properties.Resources.cupon_normal_metro;
                }
                #endregion

                #region Ajustar tamaño del Fondo de Ticket
                var tamanhoCupon = new Size(534, 1230);
                imagenCupon = (Image)(new Bitmap(imagenCupon, tamanhoCupon));
                #endregion

                #region Imagen del Codigo de Barras
                Image imagenCodigoBarra = null;
                string codigoBarra = "";
                using (var barcode = new BarcodeLib.Barcode())
                {
                    int codigoBarraLen = 13;
                    if (this.txtCodigoBarra.Text.Trim().Length == codigoBarraLen)
                        codigoBarra = this.txtCodigoBarra.Text.Trim();
                    else
                        codigoBarra = new string('0', codigoBarraLen);

                    barcode.IncludeLabel = false;
                    imagenCodigoBarra = barcode.Encode(BarcodeLib.TYPE.CODE128, codigoBarra, Color.Black, Color.White, 400, 100);
                }
                #endregion

                #region Formato de Texto
                StringFormat drawFormat = new StringFormat();
                drawFormat.Alignment = StringAlignment.Center;
                #endregion

                using (Graphics graphics = Graphics.FromImage(imagenCupon))
                {

                    #region Cabecera
                    if (this.cboTipoLegal.SelectedValue.ToString() == "1") //Bebida Alcoholicas
                    {
                        using (Font arialFont = new Font("Arial Black", 55))
                        {
                            graphics.DrawString(descuento, arialFont, Brushes.Black, 270f, 150f, drawFormat);

                        }
                        using (Font arialFont = new Font("Arial Black", 20))
                        {
                            graphics.DrawString(mensaje01, arialFont, Brushes.Black, 270f, 230f, drawFormat);
                            graphics.DrawString(mensaje02, arialFont, Brushes.Black, 270f, 265f, drawFormat);
                            graphics.DrawString(mensaje03, arialFont, Brushes.Black, 270f, 300f, drawFormat);
                        }
                    }
                    else //Normal
                    {
                        using (Font arialFont = new Font("Arial Black", 55))
                        {
                            graphics.DrawString(descuento, arialFont, Brushes.Black, 270f, 180f, drawFormat);

                        }
                        using (Font arialFont = new Font("Arial Black", 25))
                        {
                            graphics.DrawString(mensaje01, arialFont, Brushes.Black, 270f, 255f, drawFormat);
                            graphics.DrawString(mensaje02, arialFont, Brushes.Black, 270f, 290f, drawFormat);
                            graphics.DrawString(mensaje03, arialFont, Brushes.Black, 270f, 320f, drawFormat);
                        }
                    }

                    #endregion

                    #region Cuerpo

                    string mensajeCanjeTienda = "Canjéalo en la Tienda";
                    string mensajeCanjeOnline = "Canjéalo en " + this.cboAmbito.SelectedValue.ToString() + ".pe";
                    string mensajeOnlineCodigo = "Ingresando el código: ";

                    using (Font arialFont = new Font("Arial Black", 25))
                    {

                        if (this.cboCanal.SelectedValue.ToString() == "1") //Canje Tienda
                        {
                            graphics.DrawString(mensajeCanjeTienda, arialFont, Brushes.Black, 250f, 505f, drawFormat);
                            graphics.DrawImage(imagenCodigoBarra, 120, 550, 300, 100);
                            graphics.DrawString(codigoBarra, arialFont, Brushes.Black, 270f, 650f, drawFormat);

                            // Create Point for upper-left corner of image.
                            Point ulCorner = new Point(400, 400);
                            int x = 210;
                            int y = 390;
                            int width = 125;
                            int height = 125;

                            // Draw image to screen.
                            graphics.DrawImage(logoCupon, x, y, width, height);

                            if (chkBonusSeriado.Checked == true)
                            {
                                string numeroBonus = this.txtNumeroBonus.Text.Trim();
                                string bonusSeriado = this.SerialBonus(numeroBonus);

                                graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 680f, drawFormat);
                            }
                        }
                        else if (this.cboCanal.SelectedValue.ToString() == "2") //Canje Online
                        {
                            graphics.DrawString(mensajeCanjeOnline, arialFont, Brushes.Black, 250f, 560f, drawFormat);
                            graphics.DrawString(mensajeOnlineCodigo, arialFont, Brushes.Black, 250f, 590f, drawFormat);
                            graphics.DrawString(this.txtCodigoOnline.Text.Trim(), arialFont, Brushes.Black, 250f, 620f, drawFormat);

                            // Create Point for upper-left corner of image.
                            Point ulCorner = new Point(400, 400);
                            int x = 210;
                            int y = 415;
                            int width = 125;
                            int height = 125;

                            // Draw image to screen.
                            graphics.DrawImage(logoCupon, x, y, width, height);

                            if (chkBonusSeriado.Checked == true)
                            {
                                string numeroBonus = this.txtNumeroBonus.Text.Trim();
                                string bonusSeriado = this.SerialBonus(numeroBonus);

                                graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 730f, drawFormat);
                            }
                        }
                        else if (this.cboCanal.SelectedValue.ToString() == "3") //Canje Tienda y online
                        {
                            if (this.cboTipoLegal.SelectedValue.ToString() == "1") //Bebida Alcoholicas
                            {
                                graphics.DrawString(mensajeCanjeTienda, arialFont, Brushes.Black, 260f, 420f, drawFormat);
                                graphics.DrawImage(imagenCodigoBarra, 120, 465, 300, 100);
                                graphics.DrawString(codigoBarra, arialFont, Brushes.Black, 270f, 555f, drawFormat);

                                graphics.DrawString("o " + mensajeCanjeOnline, arialFont, Brushes.Black, 270f, 620f, drawFormat);
                                graphics.DrawString(mensajeOnlineCodigo, arialFont, Brushes.Black, 270f, 650f, drawFormat);
                                graphics.DrawString(this.txtCodigoOnline.Text.Trim(), arialFont, Brushes.Black, 270f, 690f, drawFormat);

                                // Create Point for upper-left corner of image.
                                Point ulCorner = new Point(400, 400);
                                int x = 210;
                                int y = 310;
                                int width = 125;
                                int height = 125;

                                // Draw image to screen.
                                graphics.DrawImage(logoCupon, x, y, width, height);

                                if (chkBonusSeriado.Checked == true)
                                {
                                    string numeroBonus = this.txtNumeroBonus.Text.Trim();
                                    string bonusSeriado = this.SerialBonus(numeroBonus);

                                    graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 590f, drawFormat);
                                }
                            }
                            else //Normal
                            {
                                graphics.DrawString(mensajeCanjeTienda, arialFont, Brushes.Black, 260f, 550f, drawFormat);

                                graphics.DrawImage(imagenCodigoBarra, 120, 600, 300, 100);
                                graphics.DrawString(codigoBarra, arialFont, Brushes.Black, 270f, 690f, drawFormat);

                                graphics.DrawString("o " + mensajeCanjeOnline, arialFont, Brushes.Black, 270f, 755f, drawFormat);
                                graphics.DrawString(mensajeOnlineCodigo, arialFont, Brushes.Black, 270f, 795f, drawFormat);
                                graphics.DrawString(this.txtCodigoOnline.Text.Trim(), arialFont, Brushes.Black, 270f, 825f, drawFormat);

                                // Create Point for upper-left corner of image.
                                Point ulCorner = new Point(400, 400);
                                int x = 210;
                                int y = 415;
                                int width = 125;
                                int height = 125;

                                // Draw image to screen.
                                graphics.DrawImage(logoCupon, x, y, width, height);

                                if (chkBonusSeriado.Checked == true)
                                {
                                    string numeroBonus = this.txtNumeroBonus.Text.Trim();
                                    string bonusSeriado = this.SerialBonus(numeroBonus);

                                    graphics.DrawString(bonusSeriado, arialFont, Brushes.Black, 270f, 725f, drawFormat);
                                }
                            }
                        }

                    }
                    #endregion

                    #region Pie - Descripcion Legal
                    using (Font arialFont = new Font("Arial Black", 12))
                    {

                        int floaty = 0;
                        if (this.cboTipoLegal.SelectedValue.ToString() == "1") //Bebida Alcoholicas
                            floaty = 730;
                        else //Normal
                            floaty = 890;

                        foreach (var line in linelegal)
                        {
                            graphics.DrawString(line.Trim(), arialFont, Brushes.Black, 270f, floaty, drawFormat);
                            floaty += 20;
                        }
                    }
                    #endregion

                    #region Lateral Izquierdo
                    using (Font arialFont = new Font("Arial Black", 15))
                    {
                        float x = 20.0F;
                        float y = 310.0F;

                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                        graphics.DrawString(this.txtVigencia.Text.Trim(), arialFont, drawBrush, x, y, drawFormat);
                    }

                    string fecha = DateTime.Now.ToString("dd/MM/yyyy");
                    string tiendaFecha = this.txtTienda.Text + " - " + fecha;
                    using (Font arialFont = new Font("Arial Black", 15))
                    {
                        float x = 20.0F;
                        float y = 700.0F;

                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                        graphics.DrawString(tiendaFecha, arialFont, drawBrush, x, y, drawFormat);
                    }
                    #endregion

                    #region Lateral Derecho

                    string docLateral = this.cboTipoDocumento.SelectedValue.ToString() + "-" + this.txtNroDocumento.Text.Trim();
                    using (Font arialFont = new Font("Arial Black", 15))
                    {
                        float x = 480.0F;
                        float y = 310.0F;

                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
                        graphics.DrawString(docLateral, arialFont, drawBrush, x, y, drawFormat);
                    }
                    #endregion

                    impresionCupon = imagenCupon;
                }

                return impresionCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ObtenerCupon(string nroDocumento, int nroCupon, int idGrupo, int tipoCupon)
        {
            try
            {
                var bl = new BL.Cupon(nroDocumento, idGrupo, tipoCupon);
                this.beCoupon = bl.Obtener(nroCupon);

                if (beCoupon != null)
                {
                    this.txtDescuento.Text = beCoupon.Descuento;
                    this.txtMensaje01.Text = beCoupon.Mensaje1;
                    this.txtMensaje02.Text = beCoupon.Mensaje2;
                    this.txtMensaje03.Text = beCoupon.Mensaje3;
                    this.cboAmbito.SelectedValue = beCoupon.Ambito;
                    this.cboTipoLegal.SelectedValue = int.Parse(beCoupon.TipoLegal);
                    this.txtMensajeLegal.Text = beCoupon.MensajeLegal;
                    this.txtUrlImagen.Text = beCoupon.UrlImagen;
                    this.txtCodigoBarra.Text = beCoupon.CodigoBarra;
                    this.cboCanal.SelectedValue = int.Parse(beCoupon.CodigoCanal);
                    this.chkBonusSeriado.Checked = (beCoupon.FlagSeriado == "1");
                    this.txtNumeroBonus.Text = beCoupon.BonusSeriado;
                    this.txtCodigoOnline.Text = beCoupon.CodigoOnline;
                    this.txtVigencia.Text = beCoupon.Vigencia;

                    int idTipoCupon = int.Parse(beCoupon.Tipo);
                    if (idTipoCupon == 0)
                        this.cboTipoCupon.SelectedValue = 2;
                    else
                    {
                        if (idTipoCupon > 0 && idTipoCupon < 2)
                            this.cboTipoCupon.SelectedValue = idTipoCupon;
                        else
                            this.cboTipoCupon.SelectedValue = 2;
                    }

                }
                else
                {
                    this.txtDescuento.Clear();
                    this.txtMensaje01.Clear();
                    this.txtMensaje02.Clear();
                    this.txtMensaje03.Clear();
                    this.cboAmbito.SelectedValue = "WONG";
                    this.cboTipoLegal.SelectedValue = "0";
                    this.txtMensajeLegal.Clear();
                    this.txtUrlImagen.Clear();
                    this.txtCodigoBarra.Clear();
                    this.cboCanal.SelectedValue = "1";
                    this.chkBonusSeriado.Checked = false;
                    this.txtNumeroBonus.Clear();
                    this.txtCodigoOnline.Clear();
                    this.txtVigencia.Clear();
                    this.cboTipoCupon.SelectedValue = 2;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
