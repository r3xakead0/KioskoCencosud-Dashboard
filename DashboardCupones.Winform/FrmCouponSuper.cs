﻿using MaterialSkin.Controls;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BL = DashboardCupones.Library.BusinessLogic;
using BE = DashboardCupones.Library.BusinessEntity;
using System.Drawing.Imaging;

namespace DashboardCupones.Winform
{
    public partial class FrmCouponSuper : MaterialForm
    {

        #region "Singletons"

        private static FrmCouponSuper frmInstance = null;

        public static FrmCouponSuper Instance()
        {

            if (frmInstance == null || frmInstance.IsDisposed == true)
            {
                frmInstance = new FrmCouponSuper();
            }

            frmInstance.BringToFront();

            return frmInstance;
        }

        #endregion

        private string ambito = "";
        private string tienda = "";
        private string nroDocumento = "";
        private int idGrupoCupon = 2;
        private int idTipoCupon = 0;

        public FrmCouponSuper()
        {
            InitializeComponent();

            Application.ThreadException += (sender, args) =>
            {
                MessageBox.Show(args.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
            {
                MessageBox.Show((args.ExceptionObject as Exception).Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            };
        }

        private void Cupoun_Load(object sender, EventArgs e)
        {
            try
            {
                #region Ambito

                DataTable dtAmbito = new DataTable();
                dtAmbito.Columns.Add("codigo", typeof(string));
                dtAmbito.Columns.Add("nombre", typeof(string));

                DataRow drAmbito = null;
                drAmbito = dtAmbito.NewRow();
                drAmbito["codigo"] = "WONG";
                drAmbito["nombre"] = "Wong";
                dtAmbito.Rows.Add(drAmbito);
                drAmbito = dtAmbito.NewRow();
                drAmbito["codigo"] = "METRO";
                drAmbito["nombre"] = "Metro";
                dtAmbito.Rows.Add(drAmbito);

                this.CargarComboBox(ref this.cboAmbito, dtAmbito);

                #endregion

                #region Tipo Cupon

                DataTable dtTipo = new DataTable();
                dtTipo.Columns.Add("codigo", typeof(int));
                dtTipo.Columns.Add("nombre", typeof(string));

                DataRow drTipo = null;
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 2;
                drTipo["nombre"] = "Normal";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 1;
                drTipo["nombre"] = "Regalo";
                dtTipo.Rows.Add(drTipo);
                drTipo = dtTipo.NewRow();
                drTipo["codigo"] = 3;
                drTipo["nombre"] = "Horario";
                dtTipo.Rows.Add(drTipo);

                this.CargarComboBox(ref this.cboTipoCupon, dtTipo);

                #endregion

                #region Tipo de Documento

                DataTable dtTipoDocumento = new DataTable();
                dtTipoDocumento.Columns.Add("codigo", typeof(string));
                dtTipoDocumento.Columns.Add("nombre", typeof(string));

                DataRow drTipoDocumento = null;
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "D";
                drTipoDocumento["nombre"] = "DNI";
                dtTipoDocumento.Rows.Add(drTipoDocumento);
                drTipoDocumento = dtTipoDocumento.NewRow();
                drTipoDocumento["codigo"] = "B";
                drTipoDocumento["nombre"] = "BONUS";
                dtTipoDocumento.Rows.Add(drTipoDocumento);

                this.CargarComboBox(ref this.cboTipoDocumento, dtTipoDocumento);

                #endregion

            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                switch (this.idTipoCupon)
                {
                    case 1: //Regalo
                        this.picSuperCoupon.Image = null;
                        break;
                    case 2: //Super
                        this.picSuperCoupon.Image = this.ObtenerSuper();
                        break;
                    case 3: //Horario
                        this.picSuperCoupon.Image = this.ObtenerHorario();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }
        
        private void btnObtener_Click(object sender, EventArgs e)
        {
            try
            {

                if (this.txtNroDocumento.Text.Trim().Length == 0)
                {
                    this.txtNroDocumento.Focus();
                    MessageBox.Show("Ingrese el número de documento", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                this.ambito = this.cboAmbito.SelectedValue.ToString();
                this.tienda = this.txtTienda.Text.Trim();
                this.nroDocumento = this.txtNroDocumento.Text.Trim();
                this.idTipoCupon = int.Parse(this.cboTipoCupon.SelectedValue.ToString());

                this.MostrarSuperCupon(this.nroDocumento, this.idGrupoCupon, this.ambito, this.tienda, this.idTipoCupon);
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }
        
        private void txtNroDocumento_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNroDocumento.Text.Trim().Length > 8)
                {
                    this.cboTipoDocumento.SelectedValue = "B";
                }
                else
                {
                    this.cboTipoDocumento.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }

        private void picCoupon_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.picSuperCoupon.Image != null)
                {
                   
                }
            }
            catch (Exception ex)
            {
                Util.ErrorMessage(ex.Message);
            }
        }


        public void MostrarSuperCupon(string nroDocumento, int idGrupo, string ambito, string tienda, int tipoCupon)
        {
            try
            {
                var bl = new BL.Cupon(nroDocumento, idGrupo, tipoCupon);
                var lstCoupons = bl.Listar(ambito, tienda);
                if (lstCoupons != null)
                {
                    if (lstCoupons.Count == 1)
                    {
                        var coupon = lstCoupons[0];

                        this.txtCupon.Text = coupon.Id.ToString();
                        this.txtDescuento.Text = coupon.Descuento;
                        this.txtMensaje01.Text = coupon.Mensaje1;
                        this.txtMensaje02.Text = coupon.Mensaje2;
                        this.txtMensaje03.Text = coupon.Mensaje3;
                        this.txtUrlImagen.Text = coupon.UrlImagen;
                        this.txtDescripcion.Text = coupon.Descripcion;

                        switch (tipoCupon)
                        {
                            case 1: //Regalo
                                this.picSuperCoupon.Image = null;
                                break;
                            case 2: //Super
                                this.picSuperCoupon.Image = this.ObtenerSuper();
                                break;
                            case 3: //Horario
                                this.picSuperCoupon.Image = this.ObtenerHorario();
                                break;
                            default:
                                break;
                        }

                        return;
                    }
                }

                this.txtCupon.Clear();
                this.txtDescuento.Clear();
                this.txtMensaje01.Clear();
                this.txtMensaje02.Clear();
                this.txtMensaje03.Clear();
                this.txtUrlImagen.Clear();
                this.txtDescripcion.Clear();

                this.picSuperCoupon.Image = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CargarComboBox(ref ComboBox cbo, DataTable dt)
        {
            try
            {
                cbo.DataSource = dt;
                cbo.DisplayMember = dt.Columns[1].Caption;
                cbo.ValueMember = dt.Columns[0].Caption;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Image CargarImagen(string url)
        {
            Image imagen = null;
            try
            {

                Uri uri = new Uri(url);
                string filename = Path.GetFileName(uri.LocalPath);

                string directory = "Cache";

                string path = Path.Combine(directory, filename);

                if (File.Exists(path) == true)
                {
                    imagen = Image.FromFile(path);
                }
                else
                {
                    if (Directory.Exists(directory) == false)
                        Directory.CreateDirectory(directory);

                    using (var client = new WebClient())
                    {
                        client.DownloadFile(url, path);
                    }
                    imagen = Image.FromFile(path);
                }


                return imagen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        private Image ObtenerSuper()
        {
            try
            {

                Image imageSuperCupon = DashboardCupones.Winform.Properties.Resources.cupon_super_wong;

                string urlImagen = this.txtUrlImagen.Text;
                Image imageContenidoSuperCupon = this.CargarImagen(urlImagen);

                string descuento = this.txtDescuento.Text;
                string mensaje1 = this.txtMensaje01.Text;
                string mensaje2 = this.txtMensaje02.Text;
                string mensaje3 = this.txtMensaje03.Text;

                #region Formato de texto
                StringFormat drawFormat = new StringFormat();
                drawFormat.Alignment = StringAlignment.Center;
                #endregion

                using (Graphics graphics = Graphics.FromImage(imageSuperCupon))
                {
                    graphics.DrawImage((Bitmap)imageContenidoSuperCupon, 20, 40, 290, 415);

                    #region Titulo
                    using (Font arialFont = new Font("Arial Black", 40))
                    {
                        graphics.DrawString(descuento, arialFont, Brushes.Orange, 170f, 400f, drawFormat);

                    }
                    #endregion

                    #region Sub Titulo
                    using (Font arialFont = new Font("Arial Black", 25))
                    {

                        graphics.DrawString(mensaje1, arialFont, Brushes.White, 170f, 480f, drawFormat);
                        graphics.DrawString(mensaje2, arialFont, Brushes.White, 170f, 510f, drawFormat);
                        graphics.DrawString(mensaje3, arialFont, Brushes.White, 170f, 540f, drawFormat);
                    }
                    #endregion

                    #region Descripcion
                    using (Font arialFont = new Font("Arial", 8))
                    {

                        string description = this.txtDescripcion.Text ?? "";
                        description = description.ToUpper();

                        int lineLength = 48;
                        string[] lineDesc = Regex.Matches(description, ".{1," + lineLength + "}")
                                            .Cast<Match>()
                                            .Select(m => m.Value)
                                            .ToArray();

                        int floaty = 664;
                        foreach (var line in lineDesc)
                        {
                            graphics.DrawString(line.Trim(), arialFont, Brushes.Black, 160f, floaty, drawFormat);
                            floaty += 15;
                        }
                    }
                    #endregion

                }

                return imageSuperCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Image ObtenerHorario()
        {
            
            try
            {

                Image imageSuperCupon = DashboardCupones.Winform.Properties.Resources.cupon_horario_wong;

                string urlImagen = this.txtUrlImagen.Text;
                Image imageContenidoSuperCupon = this.CargarImagen(urlImagen);

                string descuento = this.txtDescuento.Text;
                string mensaje1 = this.txtMensaje01.Text;
                string mensaje2 = this.txtMensaje02.Text;
                string mensaje3 = this.txtMensaje03.Text;

                #region Formato de texto
                StringFormat drawFormat = new StringFormat();
                drawFormat.Alignment = StringAlignment.Center;
                #endregion

                using (Graphics graphics = Graphics.FromImage(imageSuperCupon))
                {

                    #region Imagen

                    graphics.DrawImage((Bitmap)imageContenidoSuperCupon, 90f, 330f, 145f, 208f);

                    #endregion

                    #region Titulo
                    using (Font arialFont = new Font("Verdana", 25))
                    {
                        graphics.DrawString(descuento, arialFont, Brushes.Orange, 170f, 490f, drawFormat);

                    }
                    #endregion

                    #region Descripcion
                    using (Font arialFont = new Font("Verdana", 15))
                    {

                        graphics.DrawString(mensaje1, arialFont, Brushes.White, 170f, 540f, drawFormat);
                        graphics.DrawString(mensaje2, arialFont, Brushes.White, 170f, 560f, drawFormat);
                        graphics.DrawString(mensaje3, arialFont, Brushes.White, 170f, 580f, drawFormat);
                    }
                    #endregion

                }

                return imageSuperCupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
