﻿namespace DashboardCupones.Winform
{
    partial class FrmDashboard
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDashboard));
            this.chtExperienciasResumen = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgvKioskos = new System.Windows.Forms.DataGridView();
            this.tbcPrincipal = new MaterialSkin.Controls.MaterialTabControl();
            this.tbpKioskos = new System.Windows.Forms.TabPage();
            this.lblKioskoDesconectados = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblKioskoConectados = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chrKioskos = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnActualizarKioskos = new System.Windows.Forms.Button();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.lblKioskosActualizacion = new System.Windows.Forms.Label();
            this.tbpExperiencias = new System.Windows.Forms.TabPage();
            this.cboExperienciaHostnames = new System.Windows.Forms.ComboBox();
            this.cboExperienciaTiendas = new System.Windows.Forms.ComboBox();
            this.cboExperienciaEmpresas = new System.Windows.Forms.ComboBox();
            this.lblExperienciaTiendas = new System.Windows.Forms.Label();
            this.btnActualizarExperiencias = new System.Windows.Forms.Button();
            this.lblTotPrcExperiencias = new System.Windows.Forms.Label();
            this.lblTotCantExperiencias = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvExperiencias = new System.Windows.Forms.DataGridView();
            this.lblExperienciasActualizacion = new System.Windows.Forms.Label();
            this.chtExperienciasDetalle = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tbpClientes = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvSesionesClientes = new System.Windows.Forms.DataGridView();
            this.cboTopClientes = new System.Windows.Forms.ComboBox();
            this.lblTopClientes = new System.Windows.Forms.Label();
            this.dgvTopClientes = new System.Windows.Forms.DataGridView();
            this.btnActualizarClientes = new System.Windows.Forms.Button();
            this.chtSesionesPorTiendas = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblClientesActualizacion = new System.Windows.Forms.Label();
            this.tcsPrincipal = new MaterialSkin.Controls.MaterialTabSelector();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.btnTema = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnColor = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCuponDiseno = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCuponMiniatura = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnCuponSuper = new MaterialSkin.Controls.MaterialRaisedButton();
            ((System.ComponentModel.ISupportInitialize)(this.chtExperienciasResumen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKioskos)).BeginInit();
            this.tbcPrincipal.SuspendLayout();
            this.tbpKioskos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrKioskos)).BeginInit();
            this.tbpExperiencias.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvExperiencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtExperienciasDetalle)).BeginInit();
            this.tbpClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSesionesClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTopClientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtSesionesPorTiendas)).BeginInit();
            this.SuspendLayout();
            // 
            // chtExperienciasResumen
            // 
            this.chtExperienciasResumen.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chtExperienciasResumen.BorderlineColor = System.Drawing.Color.Black;
            this.chtExperienciasResumen.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chtExperienciasResumen.ChartAreas.Add(chartArea1);
            this.chtExperienciasResumen.Location = new System.Drawing.Point(6, 53);
            this.chtExperienciasResumen.Name = "chtExperienciasResumen";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Name = "Series1";
            this.chtExperienciasResumen.Series.Add(series1);
            this.chtExperienciasResumen.Size = new System.Drawing.Size(400, 370);
            this.chtExperienciasResumen.TabIndex = 0;
            this.chtExperienciasResumen.Text = "Torta Experiencias";
            // 
            // dgvKioskos
            // 
            this.dgvKioskos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvKioskos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKioskos.Location = new System.Drawing.Point(6, 6);
            this.dgvKioskos.Name = "dgvKioskos";
            this.dgvKioskos.Size = new System.Drawing.Size(774, 529);
            this.dgvKioskos.TabIndex = 6;
            this.dgvKioskos.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvKioskos_CellFormatting);
            // 
            // tbcPrincipal
            // 
            this.tbcPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcPrincipal.Controls.Add(this.tbpKioskos);
            this.tbcPrincipal.Controls.Add(this.tbpExperiencias);
            this.tbcPrincipal.Controls.Add(this.tbpClientes);
            this.tbcPrincipal.Depth = 0;
            this.tbcPrincipal.Location = new System.Drawing.Point(12, 129);
            this.tbcPrincipal.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbcPrincipal.Name = "tbcPrincipal";
            this.tbcPrincipal.SelectedIndex = 0;
            this.tbcPrincipal.Size = new System.Drawing.Size(1000, 593);
            this.tbcPrincipal.TabIndex = 12;
            // 
            // tbpKioskos
            // 
            this.tbpKioskos.Controls.Add(this.lblKioskoDesconectados);
            this.tbpKioskos.Controls.Add(this.label7);
            this.tbpKioskos.Controls.Add(this.lblKioskoConectados);
            this.tbpKioskos.Controls.Add(this.label5);
            this.tbpKioskos.Controls.Add(this.label3);
            this.tbpKioskos.Controls.Add(this.chrKioskos);
            this.tbpKioskos.Controls.Add(this.btnActualizarKioskos);
            this.tbpKioskos.Controls.Add(this.btnReiniciar);
            this.tbpKioskos.Controls.Add(this.dgvKioskos);
            this.tbpKioskos.Controls.Add(this.lblKioskosActualizacion);
            this.tbpKioskos.Location = new System.Drawing.Point(4, 22);
            this.tbpKioskos.Name = "tbpKioskos";
            this.tbpKioskos.Padding = new System.Windows.Forms.Padding(3);
            this.tbpKioskos.Size = new System.Drawing.Size(992, 567);
            this.tbpKioskos.TabIndex = 0;
            this.tbpKioskos.Text = "Kioskos";
            this.tbpKioskos.UseVisualStyleBackColor = true;
            // 
            // lblKioskoDesconectados
            // 
            this.lblKioskoDesconectados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKioskoDesconectados.Location = new System.Drawing.Point(933, 326);
            this.lblKioskoDesconectados.Name = "lblKioskoDesconectados";
            this.lblKioskoDesconectados.Size = new System.Drawing.Size(50, 18);
            this.lblKioskoDesconectados.TabIndex = 24;
            this.lblKioskoDesconectados.Text = "0";
            this.lblKioskoDesconectados.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(783, 326);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(144, 18);
            this.label7.TabIndex = 23;
            this.label7.Text = "Desconectados :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblKioskoConectados
            // 
            this.lblKioskoConectados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKioskoConectados.Location = new System.Drawing.Point(933, 308);
            this.lblKioskoConectados.Name = "lblKioskoConectados";
            this.lblKioskoConectados.Size = new System.Drawing.Size(50, 18);
            this.lblKioskoConectados.TabIndex = 22;
            this.lblKioskoConectados.Text = "0";
            this.lblKioskoConectados.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(783, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 18);
            this.label5.TabIndex = 21;
            this.label5.Text = "Conectados :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Navy;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(786, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 21);
            this.label3.TabIndex = 18;
            this.label3.Text = "Resumen";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chrKioskos
            // 
            this.chrKioskos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chrKioskos.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chrKioskos.BorderlineColor = System.Drawing.Color.Black;
            this.chrKioskos.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.Name = "ChartArea1";
            this.chrKioskos.ChartAreas.Add(chartArea2);
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chrKioskos.Legends.Add(legend1);
            this.chrKioskos.Location = new System.Drawing.Point(786, 30);
            this.chrKioskos.Name = "chrKioskos";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chrKioskos.Series.Add(series2);
            this.chrKioskos.Size = new System.Drawing.Size(200, 275);
            this.chrKioskos.TabIndex = 10;
            // 
            // btnActualizarKioskos
            // 
            this.btnActualizarKioskos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnActualizarKioskos.Location = new System.Drawing.Point(87, 538);
            this.btnActualizarKioskos.Name = "btnActualizarKioskos";
            this.btnActualizarKioskos.Size = new System.Drawing.Size(75, 23);
            this.btnActualizarKioskos.TabIndex = 9;
            this.btnActualizarKioskos.Text = "Actualizar";
            this.btnActualizarKioskos.UseVisualStyleBackColor = true;
            this.btnActualizarKioskos.Click += new System.EventHandler(this.btnActualizarKioskos_Click);
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReiniciar.Location = new System.Drawing.Point(6, 538);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(75, 23);
            this.btnReiniciar.TabIndex = 8;
            this.btnReiniciar.Text = "Reiniciar";
            this.btnReiniciar.UseVisualStyleBackColor = true;
            this.btnReiniciar.Click += new System.EventHandler(this.btnReiniciar_Click);
            // 
            // lblKioskosActualizacion
            // 
            this.lblKioskosActualizacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblKioskosActualizacion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKioskosActualizacion.Location = new System.Drawing.Point(700, 538);
            this.lblKioskosActualizacion.Name = "lblKioskosActualizacion";
            this.lblKioskosActualizacion.Size = new System.Drawing.Size(286, 23);
            this.lblKioskosActualizacion.TabIndex = 7;
            this.lblKioskosActualizacion.Text = "Ultima actualización 14/06/2018 18:09:10";
            this.lblKioskosActualizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbpExperiencias
            // 
            this.tbpExperiencias.Controls.Add(this.cboExperienciaHostnames);
            this.tbpExperiencias.Controls.Add(this.cboExperienciaTiendas);
            this.tbpExperiencias.Controls.Add(this.cboExperienciaEmpresas);
            this.tbpExperiencias.Controls.Add(this.lblExperienciaTiendas);
            this.tbpExperiencias.Controls.Add(this.btnActualizarExperiencias);
            this.tbpExperiencias.Controls.Add(this.lblTotPrcExperiencias);
            this.tbpExperiencias.Controls.Add(this.lblTotCantExperiencias);
            this.tbpExperiencias.Controls.Add(this.label1);
            this.tbpExperiencias.Controls.Add(this.dgvExperiencias);
            this.tbpExperiencias.Controls.Add(this.lblExperienciasActualizacion);
            this.tbpExperiencias.Controls.Add(this.chtExperienciasDetalle);
            this.tbpExperiencias.Controls.Add(this.chtExperienciasResumen);
            this.tbpExperiencias.Location = new System.Drawing.Point(4, 22);
            this.tbpExperiencias.Name = "tbpExperiencias";
            this.tbpExperiencias.Padding = new System.Windows.Forms.Padding(3);
            this.tbpExperiencias.Size = new System.Drawing.Size(992, 567);
            this.tbpExperiencias.TabIndex = 1;
            this.tbpExperiencias.Text = "Experiencias";
            this.tbpExperiencias.UseVisualStyleBackColor = true;
            // 
            // cboExperienciaHostnames
            // 
            this.cboExperienciaHostnames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboExperienciaHostnames.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboExperienciaHostnames.FormattingEnabled = true;
            this.cboExperienciaHostnames.Location = new System.Drawing.Point(276, 26);
            this.cboExperienciaHostnames.Name = "cboExperienciaHostnames";
            this.cboExperienciaHostnames.Size = new System.Drawing.Size(130, 21);
            this.cboExperienciaHostnames.TabIndex = 22;
            this.cboExperienciaHostnames.SelectionChangeCommitted += new System.EventHandler(this.cboExperienciaHostnames_SelectionChangeCommitted);
            // 
            // cboExperienciaTiendas
            // 
            this.cboExperienciaTiendas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboExperienciaTiendas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboExperienciaTiendas.FormattingEnabled = true;
            this.cboExperienciaTiendas.Location = new System.Drawing.Point(142, 26);
            this.cboExperienciaTiendas.Name = "cboExperienciaTiendas";
            this.cboExperienciaTiendas.Size = new System.Drawing.Size(130, 21);
            this.cboExperienciaTiendas.TabIndex = 21;
            this.cboExperienciaTiendas.SelectionChangeCommitted += new System.EventHandler(this.cboExperienciaTiendas_SelectionChangeCommitted);
            // 
            // cboExperienciaEmpresas
            // 
            this.cboExperienciaEmpresas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboExperienciaEmpresas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboExperienciaEmpresas.FormattingEnabled = true;
            this.cboExperienciaEmpresas.Location = new System.Drawing.Point(6, 26);
            this.cboExperienciaEmpresas.Name = "cboExperienciaEmpresas";
            this.cboExperienciaEmpresas.Size = new System.Drawing.Size(130, 21);
            this.cboExperienciaEmpresas.TabIndex = 20;
            this.cboExperienciaEmpresas.SelectionChangeCommitted += new System.EventHandler(this.cboExperienciaEmpresas_SelectionChangeCommitted);
            // 
            // lblExperienciaTiendas
            // 
            this.lblExperienciaTiendas.BackColor = System.Drawing.Color.Navy;
            this.lblExperienciaTiendas.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExperienciaTiendas.ForeColor = System.Drawing.Color.White;
            this.lblExperienciaTiendas.Location = new System.Drawing.Point(6, 6);
            this.lblExperienciaTiendas.Name = "lblExperienciaTiendas";
            this.lblExperienciaTiendas.Size = new System.Drawing.Size(400, 17);
            this.lblExperienciaTiendas.TabIndex = 19;
            this.lblExperienciaTiendas.Text = "Experiencia por Tienda";
            this.lblExperienciaTiendas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnActualizarExperiencias
            // 
            this.btnActualizarExperiencias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnActualizarExperiencias.Location = new System.Drawing.Point(6, 538);
            this.btnActualizarExperiencias.Name = "btnActualizarExperiencias";
            this.btnActualizarExperiencias.Size = new System.Drawing.Size(75, 23);
            this.btnActualizarExperiencias.TabIndex = 14;
            this.btnActualizarExperiencias.Text = "Actualizar";
            this.btnActualizarExperiencias.UseVisualStyleBackColor = true;
            this.btnActualizarExperiencias.Click += new System.EventHandler(this.btnActualizarExperiencias_Click);
            // 
            // lblTotPrcExperiencias
            // 
            this.lblTotPrcExperiencias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTotPrcExperiencias.Location = new System.Drawing.Point(327, 517);
            this.lblTotPrcExperiencias.Name = "lblTotPrcExperiencias";
            this.lblTotPrcExperiencias.Size = new System.Drawing.Size(79, 18);
            this.lblTotPrcExperiencias.TabIndex = 13;
            this.lblTotPrcExperiencias.Text = "0";
            this.lblTotPrcExperiencias.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotCantExperiencias
            // 
            this.lblTotCantExperiencias.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTotCantExperiencias.Location = new System.Drawing.Point(242, 517);
            this.lblTotCantExperiencias.Name = "lblTotCantExperiencias";
            this.lblTotCantExperiencias.Size = new System.Drawing.Size(79, 18);
            this.lblTotCantExperiencias.TabIndex = 12;
            this.lblTotCantExperiencias.Text = "0";
            this.lblTotCantExperiencias.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(157, 517);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Totales :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgvExperiencias
            // 
            this.dgvExperiencias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvExperiencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvExperiencias.Location = new System.Drawing.Point(6, 429);
            this.dgvExperiencias.Name = "dgvExperiencias";
            this.dgvExperiencias.Size = new System.Drawing.Size(400, 85);
            this.dgvExperiencias.TabIndex = 9;
            // 
            // lblExperienciasActualizacion
            // 
            this.lblExperienciasActualizacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExperienciasActualizacion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExperienciasActualizacion.Location = new System.Drawing.Point(700, 538);
            this.lblExperienciasActualizacion.Name = "lblExperienciasActualizacion";
            this.lblExperienciasActualizacion.Size = new System.Drawing.Size(286, 23);
            this.lblExperienciasActualizacion.TabIndex = 8;
            this.lblExperienciasActualizacion.Text = "Ultima actualización 14/06/2018 18:09:10";
            this.lblExperienciasActualizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chtExperienciasDetalle
            // 
            this.chtExperienciasDetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chtExperienciasDetalle.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chtExperienciasDetalle.BorderlineColor = System.Drawing.Color.Black;
            this.chtExperienciasDetalle.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea3.Name = "ChartArea1";
            this.chtExperienciasDetalle.ChartAreas.Add(chartArea3);
            this.chtExperienciasDetalle.Location = new System.Drawing.Point(419, 6);
            this.chtExperienciasDetalle.Name = "chtExperienciasDetalle";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series3.Name = "Series1";
            this.chtExperienciasDetalle.Series.Add(series3);
            this.chtExperienciasDetalle.Size = new System.Drawing.Size(567, 529);
            this.chtExperienciasDetalle.TabIndex = 4;
            this.chtExperienciasDetalle.Text = "Torta Experiencias";
            // 
            // tbpClientes
            // 
            this.tbpClientes.Controls.Add(this.label2);
            this.tbpClientes.Controls.Add(this.label4);
            this.tbpClientes.Controls.Add(this.dgvSesionesClientes);
            this.tbpClientes.Controls.Add(this.cboTopClientes);
            this.tbpClientes.Controls.Add(this.lblTopClientes);
            this.tbpClientes.Controls.Add(this.dgvTopClientes);
            this.tbpClientes.Controls.Add(this.btnActualizarClientes);
            this.tbpClientes.Controls.Add(this.chtSesionesPorTiendas);
            this.tbpClientes.Controls.Add(this.lblClientesActualizacion);
            this.tbpClientes.Location = new System.Drawing.Point(4, 22);
            this.tbpClientes.Name = "tbpClientes";
            this.tbpClientes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpClientes.Size = new System.Drawing.Size(992, 567);
            this.tbpClientes.TabIndex = 2;
            this.tbpClientes.Text = "Clientes";
            this.tbpClientes.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Navy;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(497, 21);
            this.label2.TabIndex = 21;
            this.label2.Text = "Sesiones por Tienda";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.BackColor = System.Drawing.Color.Navy;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(509, 282);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(477, 21);
            this.label4.TabIndex = 20;
            this.label4.Text = "Sesiones del Cliente";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvSesionesClientes
            // 
            this.dgvSesionesClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSesionesClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSesionesClientes.Location = new System.Drawing.Point(509, 306);
            this.dgvSesionesClientes.Name = "dgvSesionesClientes";
            this.dgvSesionesClientes.Size = new System.Drawing.Size(477, 229);
            this.dgvSesionesClientes.TabIndex = 19;
            // 
            // cboTopClientes
            // 
            this.cboTopClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTopClientes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTopClientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTopClientes.FormattingEnabled = true;
            this.cboTopClientes.Items.AddRange(new object[] {
            "25",
            "50",
            "100"});
            this.cboTopClientes.Location = new System.Drawing.Point(921, 6);
            this.cboTopClientes.Name = "cboTopClientes";
            this.cboTopClientes.Size = new System.Drawing.Size(65, 21);
            this.cboTopClientes.TabIndex = 18;
            this.cboTopClientes.SelectionChangeCommitted += new System.EventHandler(this.cboTopClientes_SelectionChangeCommitted);
            // 
            // lblTopClientes
            // 
            this.lblTopClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTopClientes.BackColor = System.Drawing.Color.Navy;
            this.lblTopClientes.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTopClientes.ForeColor = System.Drawing.Color.White;
            this.lblTopClientes.Location = new System.Drawing.Point(509, 6);
            this.lblTopClientes.Name = "lblTopClientes";
            this.lblTopClientes.Size = new System.Drawing.Size(406, 21);
            this.lblTopClientes.TabIndex = 17;
            this.lblTopClientes.Text = "Top de Clientes";
            this.lblTopClientes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvTopClientes
            // 
            this.dgvTopClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvTopClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTopClientes.Location = new System.Drawing.Point(509, 30);
            this.dgvTopClientes.Name = "dgvTopClientes";
            this.dgvTopClientes.Size = new System.Drawing.Size(477, 229);
            this.dgvTopClientes.TabIndex = 16;
            this.dgvTopClientes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTopClientes_CellClick);
            // 
            // btnActualizarClientes
            // 
            this.btnActualizarClientes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnActualizarClientes.Location = new System.Drawing.Point(6, 538);
            this.btnActualizarClientes.Name = "btnActualizarClientes";
            this.btnActualizarClientes.Size = new System.Drawing.Size(75, 23);
            this.btnActualizarClientes.TabIndex = 15;
            this.btnActualizarClientes.Text = "Actualizar";
            this.btnActualizarClientes.UseVisualStyleBackColor = true;
            this.btnActualizarClientes.Click += new System.EventHandler(this.btnActualizarClientes_Click);
            // 
            // chtSesionesPorTiendas
            // 
            this.chtSesionesPorTiendas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.chtSesionesPorTiendas.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chtSesionesPorTiendas.BorderlineColor = System.Drawing.Color.Black;
            this.chtSesionesPorTiendas.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea4.Name = "ChartArea1";
            this.chtSesionesPorTiendas.ChartAreas.Add(chartArea4);
            this.chtSesionesPorTiendas.Location = new System.Drawing.Point(6, 30);
            this.chtSesionesPorTiendas.Name = "chtSesionesPorTiendas";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series4.Name = "Series1";
            this.chtSesionesPorTiendas.Series.Add(series4);
            this.chtSesionesPorTiendas.Size = new System.Drawing.Size(497, 505);
            this.chtSesionesPorTiendas.TabIndex = 10;
            this.chtSesionesPorTiendas.Text = "Sesiones por Tiendas";
            // 
            // lblClientesActualizacion
            // 
            this.lblClientesActualizacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblClientesActualizacion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClientesActualizacion.Location = new System.Drawing.Point(700, 538);
            this.lblClientesActualizacion.Name = "lblClientesActualizacion";
            this.lblClientesActualizacion.Size = new System.Drawing.Size(286, 23);
            this.lblClientesActualizacion.TabIndex = 9;
            this.lblClientesActualizacion.Text = "Ultima actualización 14/06/2018 18:09:10";
            this.lblClientesActualizacion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tcsPrincipal
            // 
            this.tcsPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcsPrincipal.BaseTabControl = this.tbcPrincipal;
            this.tcsPrincipal.Depth = 0;
            this.tcsPrincipal.Location = new System.Drawing.Point(0, 64);
            this.tcsPrincipal.MouseState = MaterialSkin.MouseState.HOVER;
            this.tcsPrincipal.Name = "tcsPrincipal";
            this.tcsPrincipal.Size = new System.Drawing.Size(1024, 48);
            this.tcsPrincipal.TabIndex = 13;
            this.tcsPrincipal.Text = "materialTabSelector1";
            // 
            // materialDivider1
            // 
            this.materialDivider1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(0, 725);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(1024, 1);
            this.materialDivider1.TabIndex = 14;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // btnTema
            // 
            this.btnTema.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTema.Depth = 0;
            this.btnTema.Location = new System.Drawing.Point(873, 732);
            this.btnTema.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnTema.Name = "btnTema";
            this.btnTema.Primary = true;
            this.btnTema.Size = new System.Drawing.Size(139, 24);
            this.btnTema.TabIndex = 15;
            this.btnTema.Text = "Tema";
            this.btnTema.UseVisualStyleBackColor = true;
            this.btnTema.Click += new System.EventHandler(this.btnTema_Click);
            // 
            // btnColor
            // 
            this.btnColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnColor.Depth = 0;
            this.btnColor.Location = new System.Drawing.Point(728, 732);
            this.btnColor.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnColor.Name = "btnColor";
            this.btnColor.Primary = true;
            this.btnColor.Size = new System.Drawing.Size(139, 24);
            this.btnColor.TabIndex = 16;
            this.btnColor.Text = "Color";
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnCuponDiseno
            // 
            this.btnCuponDiseno.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCuponDiseno.Depth = 0;
            this.btnCuponDiseno.Location = new System.Drawing.Point(302, 732);
            this.btnCuponDiseno.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCuponDiseno.Name = "btnCuponDiseno";
            this.btnCuponDiseno.Primary = true;
            this.btnCuponDiseno.Size = new System.Drawing.Size(139, 24);
            this.btnCuponDiseno.TabIndex = 17;
            this.btnCuponDiseno.Text = "Diseño Cupon";
            this.btnCuponDiseno.UseVisualStyleBackColor = true;
            this.btnCuponDiseno.Click += new System.EventHandler(this.btnCuponDiseno_Click);
            // 
            // btnCuponMiniatura
            // 
            this.btnCuponMiniatura.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCuponMiniatura.Depth = 0;
            this.btnCuponMiniatura.Location = new System.Drawing.Point(12, 732);
            this.btnCuponMiniatura.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCuponMiniatura.Name = "btnCuponMiniatura";
            this.btnCuponMiniatura.Primary = true;
            this.btnCuponMiniatura.Size = new System.Drawing.Size(139, 24);
            this.btnCuponMiniatura.TabIndex = 18;
            this.btnCuponMiniatura.Text = "Miniatura Cupon";
            this.btnCuponMiniatura.UseVisualStyleBackColor = true;
            this.btnCuponMiniatura.Click += new System.EventHandler(this.btnCuponMiniatura_Click);
            // 
            // btnCuponSuper
            // 
            this.btnCuponSuper.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCuponSuper.Depth = 0;
            this.btnCuponSuper.Location = new System.Drawing.Point(157, 732);
            this.btnCuponSuper.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnCuponSuper.Name = "btnCuponSuper";
            this.btnCuponSuper.Primary = true;
            this.btnCuponSuper.Size = new System.Drawing.Size(139, 24);
            this.btnCuponSuper.TabIndex = 19;
            this.btnCuponSuper.Text = "Super Cupon";
            this.btnCuponSuper.UseVisualStyleBackColor = true;
            this.btnCuponSuper.Click += new System.EventHandler(this.btnCuponSuper_Click);
            // 
            // FrmDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.btnCuponSuper);
            this.Controls.Add(this.btnCuponMiniatura);
            this.Controls.Add(this.btnCuponDiseno);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.btnTema);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.tcsPrincipal);
            this.Controls.Add(this.tbcPrincipal);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DASHBOARD";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chtExperienciasResumen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKioskos)).EndInit();
            this.tbcPrincipal.ResumeLayout(false);
            this.tbpKioskos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chrKioskos)).EndInit();
            this.tbpExperiencias.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvExperiencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtExperienciasDetalle)).EndInit();
            this.tbpClientes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSesionesClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTopClientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtSesionesPorTiendas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chtExperienciasResumen;
        private System.Windows.Forms.DataGridView dgvKioskos;
        private MaterialSkin.Controls.MaterialTabControl tbcPrincipal;
        private System.Windows.Forms.TabPage tbpKioskos;
        private System.Windows.Forms.TabPage tbpExperiencias;
        private System.Windows.Forms.Label lblKioskosActualizacion;
        private System.Windows.Forms.Button btnReiniciar;
        private System.Windows.Forms.Button btnActualizarKioskos;
        private MaterialSkin.Controls.MaterialTabSelector tcsPrincipal;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private MaterialSkin.Controls.MaterialRaisedButton btnTema;
        private MaterialSkin.Controls.MaterialRaisedButton btnColor;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtExperienciasDetalle;
        private System.Windows.Forms.Label lblExperienciasActualizacion;
        private System.Windows.Forms.DataGridView dgvExperiencias;
        private System.Windows.Forms.Label lblTotPrcExperiencias;
        private System.Windows.Forms.Label lblTotCantExperiencias;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tbpClientes;
        private System.Windows.Forms.Label lblClientesActualizacion;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtSesionesPorTiendas;
        private System.Windows.Forms.Button btnActualizarExperiencias;
        private System.Windows.Forms.DataGridView dgvTopClientes;
        private System.Windows.Forms.Button btnActualizarClientes;
        private System.Windows.Forms.ComboBox cboTopClientes;
        private System.Windows.Forms.Label lblTopClientes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrKioskos;
        private System.Windows.Forms.Label lblKioskoDesconectados;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblKioskoConectados;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvSesionesClientes;
        private System.Windows.Forms.ComboBox cboExperienciaEmpresas;
        private System.Windows.Forms.Label lblExperienciaTiendas;
        private System.Windows.Forms.ComboBox cboExperienciaHostnames;
        private System.Windows.Forms.ComboBox cboExperienciaTiendas;
        private MaterialSkin.Controls.MaterialRaisedButton btnCuponDiseno;
        private MaterialSkin.Controls.MaterialRaisedButton btnCuponMiniatura;
        private System.Windows.Forms.Label label2;
        private MaterialSkin.Controls.MaterialRaisedButton btnCuponSuper;
    }
}

