﻿using System;
using System.Collections.Generic;
using BE = DashboardCupones.Library.BusinessEntity;
using DA = DashboardCupones.Library.DataAccess;
using DN = DashboardCupones.Library.NetworkAccess;

namespace DashboardCupones.Library.BusinessLogic
{
    public class Kiosko
    {
        public Kiosko()
        {

        }

        public DateTime UltimoMonitoreo()
        {
            try
            {
                return new DA.Kiosko().FechaHoraUltimoMonitoreo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.Kiosko> Monitoreo(DateTime fechaHora)
        {
            try
            {
                var lstBeKioskos = new DA.Kiosko().ListarMonitoreo(fechaHora);

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime Registrar()
        {
            try
            {

                var daKiosko = new DA.Kiosko();

                var lstBeKioskos = daKiosko.Listar();
                for (int i = 0; i < lstBeKioskos.Count; i++)
                {
                    lstBeKioskos[i] = Estado(lstBeKioskos[i]);
                }

                DateTime fechaHora = DateTime.Now;

                foreach (var beKiosko in lstBeKioskos)
                {
                    daKiosko.RegistrarMonitoreo(beKiosko, fechaHora);
                }

                return fechaHora;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE.Kiosko Estado(BE.Kiosko beKiosko)
        {
            try
            {
                BE.Kiosko kiosko = null;

                if (beKiosko != null)
                {
                    DN.Kiosko dnKiosko = new DN.Kiosko(beKiosko.Hostname);

                    kiosko = beKiosko;
                    kiosko.Conexion = dnKiosko.Conexion;
                    kiosko.Version = dnKiosko.Version;
                    kiosko.Sesion = dnKiosko.Sesion;
                    kiosko.Impreso = dnKiosko.Impreso;
                    kiosko.Restante = dnKiosko.Restante;

                }

                return kiosko;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.General> ListaSimpleEmpresas()
        {
            try
            {
                var lst = new DA.Kiosko().ListaSimpleEmpresas();
                lst.Insert(0, new BE.General() { Codigo = "", Nombre = "Todos" });
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.General> ListaSimpleTiendas(string empresa = "")
        {
            try
            {
                var lst = new List<BE.General>();
                if (empresa.Length > 0)
                {
                    lst = new DA.Kiosko().ListaSimpleTiendas(empresa);
                    lst.Insert(0, new BE.General() { Codigo = "", Nombre = "Todos" });
                }
                else
                {
                    lst.Add(new BE.General() { Codigo = "", Nombre = "Todos" });
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.General> ListaSimpleHostnames(string tienda = "")
        {
            try
            {
                var lst = new List<BE.General>();
                if (tienda.Length > 0)
                {
                    lst = new DA.Kiosko().ListaSimpleHostnames(tienda);
                    lst.Insert(0, new BE.General() { Codigo = "", Nombre = "Todos" });
                }
                else
                {
                    lst.Add(new BE.General() { Codigo = "", Nombre = "Todos" });
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
