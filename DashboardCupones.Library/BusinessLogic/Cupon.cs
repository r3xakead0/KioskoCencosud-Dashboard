﻿using System;
using System.Collections.Generic;
using BE = DashboardCupones.Library.BusinessEntity;
using DA = DashboardCupones.Library.DataAccess;

namespace DashboardCupones.Library.BusinessLogic
{
    public class Cupon
    {
        private string nroDocumento = "";
        private int idGrupo = 0;
        private int tipo = 0;

        public Cupon(string nroDocumento, int idGrupo, int tipo = 0)
        {
            this.nroDocumento = nroDocumento;
            this.idGrupo = idGrupo;
            this.tipo = tipo;
        }

        public List<BE.Cupon> Listar(string ambito, string tienda)
        {
            try
            {
                return new DA.Cupon().Listar(this.nroDocumento, this.idGrupo, ambito, tienda, this.tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE.Cupon Obtener(int idCupon)
        {
            try
            {
                return new DA.Cupon().Obtener(this.nroDocumento, idCupon, this.idGrupo, this.tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Actualizar(BE.Cupon cupon)
        {
            try
            {
                return new DA.Cupon().Actualizar(cupon);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
