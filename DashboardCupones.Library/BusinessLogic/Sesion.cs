﻿using System;
using System.Collections.Generic;
using System.Linq;
using BE = DashboardCupones.Library.BusinessEntity;
using DA = DashboardCupones.Library.DataAccess;

namespace DashboardCupones.Library.BusinessLogic
{
    public class Sesion
    {

        public Sesion()
        {

        }

        public List<BE.SesionTienda> PorTiendas(DateTime fechaInicio, DateTime fechaFinal)
        {
            try
            {
                var lstBeSesionesTiendas = new DA.Sesion().PorTiendas(fechaInicio, fechaFinal);
                double total = lstBeSesionesTiendas.Sum(x => x.Cantidad);

                for (int i = 0; i < lstBeSesionesTiendas.Count; i++)
                {
                    lstBeSesionesTiendas[i].Porcentaje = (lstBeSesionesTiendas[i].Cantidad / total) * 100;
                }

                return lstBeSesionesTiendas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.SesionCliente> TopClientes(DateTime fechaInicio, DateTime fechaFinal, int limite = 0)
        {
            try
            {
                var lstBeSesionesClientes = new DA.Sesion().PorClientes(fechaInicio, fechaFinal, limite);

                for (int i = 0; i < lstBeSesionesClientes.Count; i++)
                {
                    string num = lstBeSesionesClientes[i].NroDocumento.Trim();
                    lstBeSesionesClientes[i].TipoDocumento = num.Length == 8 ? "DNI" : "BONUS";
                }

                return lstBeSesionesClientes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.Sesion> Destalle(DateTime fechaInicio, DateTime fechaFinal, string nroDocumento)
        {
            try
            {
                var lstBeSesiones = new DA.Sesion().Detalle(fechaInicio, fechaFinal, nroDocumento);

                return lstBeSesiones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.SesionPorCliente> ResumenPorCliente(int tipoDocumento, string nroDocumento)
        {
            try
            {
                var lstSesiones = new DA.Sesion().ResumenPorCliente(tipoDocumento, nroDocumento);

                return lstSesiones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.TendenciaSesionPorCliente> TendenciaPorCliente(int tipoDocumento, string nroDocumento)
        {
            try
            {
                var lstTendeciaSesion = new List<BE.TendenciaSesionPorCliente>();

                var lstTendeciaSesionTemp = new DA.Sesion().TendenciaPorCliente(tipoDocumento, nroDocumento);
                for (int i = 1; i < 5; i++) //Ultimos 4 meses
                {
                    DateTime fecha = DateTime.Now.AddMonths(0 - i);
                    int anho = fecha.Year;
                    int mes = fecha.Month;

                    List<BE.TendenciaSesionPorCliente> lstTendencia = lstTendeciaSesionTemp.Where(x => x.Anho == anho && x.MesId == mes).ToList();
   
                    if (lstTendencia.Count == 0)
                    {
                        var tendencia = new BE.TendenciaSesionPorCliente();
                        tendencia.Anho = anho;
                        tendencia.MesId = mes;
                        tendencia.Tienda = "";
                        tendencia.Cantidad = 0;
                        tendencia.MesNombre = this.ObtenerMes(mes);

                        lstTendeciaSesion.Add(tendencia);
                    }
                    else
                    {
                        foreach (var tendencia in lstTendencia)
                        {
                            tendencia.MesNombre = this.ObtenerMes(mes);
                            lstTendeciaSesion.Add(tendencia);
                        }
                    }
                    
                }

                return lstTendeciaSesion.OrderBy(x => x.Anho).ThenBy(y => y.MesId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string ObtenerMes(int id)
        {
            string mes = "";
            switch (id)
            {
                case 1:
                    mes = "Enero";
                    break;

                case 2:
                    mes = "Febrero";
                    break;

                case 3:
                    mes = "Marzo";
                    break;

                case 4:
                    mes = "Abril";
                    break;

                case 5:
                    mes = "Mayo";
                    break;

                case 6:
                    mes = "Junio";
                    break;

                case 7:
                    mes = "Julio";
                    break;

                case 8:
                    mes = "Agosto";
                    break;

                case 9:
                    mes = "Septiembre";
                    break;

                case 10:
                    mes = "Octubre";
                    break;

                case 11:
                    mes = "Noviembre";
                    break;

                case 12:
                    mes = "Diciembre";
                    break;

            };

            return mes;
        }

    }
}
