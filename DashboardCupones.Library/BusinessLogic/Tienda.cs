﻿using System;
using System.Collections.Generic;
using System.Linq;
using BE = DashboardCupones.Library.BusinessEntity;
using DA = DashboardCupones.Library.DataAccess;

namespace DashboardCupones.Library.BusinessLogic
{
    public class Tienda
    {
        
        public List<BE.Tienda> Listar()
        {
            try
            {
                List<BE.Tienda> lstTiendas = new DA.Tienda().Listar();

                var daKiosko = new DA.Kiosko();
                DateTime fechaHoraMonitoreo = daKiosko.FechaHoraUltimoMonitoreo();

                var daSesion = new DA.Sesion();
                var daExperiencia = new DA.Experiencia();

                for (int i = 0; i < lstTiendas.Count; i++)
                {
                    string codTienda = lstTiendas[i].Codigo;
                    
                    var lstKioskos = daKiosko.ObtenerMonitoreoPorTienda(codTienda, fechaHoraMonitoreo);

                    bool conectado = false;
                    int promedioSesiones = 0;
                    int promedioClientes = 0;
                    int promedioSatisfaccion = 0;
                    int diasInactividad = 0;

                    int cntKioskos = lstKioskos.Count;

                    if (cntKioskos > 0)
                    {
                        int cntNoConectados = lstKioskos.Where(x => x.Conexion.Equals("NO")).Count();
                        conectado = cntNoConectados > 0 ? false : true;
                        promedioSesiones = daSesion.PromedioSesionesPorTienda(codTienda);
                        promedioClientes = daSesion.PromedioClientesPorTienda(codTienda);
                        promedioSatisfaccion = daExperiencia.PromedioPorTienda(codTienda);

                        if (conectado == false)
                        {
                            var fechaHoraUltimaConexion = daKiosko.FechaHoraUltimaConexionPorTienda(codTienda);
                            if (fechaHoraUltimaConexion != null)
                            {
                                DateTime fechaHoraMonitoreoPorTienda = (DateTime)fechaHoraUltimaConexion;
                                diasInactividad = (fechaHoraMonitoreo - fechaHoraMonitoreoPorTienda).Days;
                            }
                            
                        }
                    }

                    lstTiendas[i].Kioskos = cntKioskos;
                    lstTiendas[i].Conectado = conectado;
                    lstTiendas[i].PromedioSesiones = promedioSesiones;
                    lstTiendas[i].PromedioClientes = promedioClientes;
                    lstTiendas[i].PromedioSatisfaccion = promedioSatisfaccion;
                    lstTiendas[i].DiasInactividad = diasInactividad;
                }

                return lstTiendas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Registrar(BE.Tienda tienda)
        {
            try
            {
                bool rpta = false;
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Actualizar(BE.Tienda tienda)
        {
            try
            {
                bool rpta = false;
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Eliminar(string codigo)
        {
            try
            {
                bool rpta = false;
                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
