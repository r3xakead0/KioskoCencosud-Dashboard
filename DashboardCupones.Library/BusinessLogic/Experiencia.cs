﻿using System;
using System.Collections.Generic;
using System.Linq;
using BE = DashboardCupones.Library.BusinessEntity;
using DA = DashboardCupones.Library.DataAccess;

namespace DashboardCupones.Library.BusinessLogic
{
    public class Experiencia
    {

        public Experiencia()
        {

        }

        public List<BE.Experiencia> Resumen(DateTime fechaInicial,
                                            DateTime fechaFinal,
                                            string empresa = "", 
                                            string tienda = "",
                                            string hostname = "")
        {
            try
            {
                var lstBeExperiencias = new DA.Experiencia().Resumen(fechaInicial, fechaFinal, empresa, tienda, hostname);
                double total = lstBeExperiencias.Sum(x => x.Cantidad);

                for (int i = 0; i < lstBeExperiencias.Count; i++)
                {
                    lstBeExperiencias[i].Nombre = this.ObtenerNombre(lstBeExperiencias[i].Id);
                    lstBeExperiencias[i].Porcentaje = (lstBeExperiencias[i].Cantidad / total) * 100;
                }

                return lstBeExperiencias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.ExperienciaPorCliente> ResumenPorCliente(int tipoDocumento, string nroDocumento)
        {
            try
            {
                var lstExperiencias = new DA.Experiencia().ResumenPorCliente(tipoDocumento, nroDocumento);

                for (int i = 0; i < lstExperiencias.Count; i++)
                {
                    var id = lstExperiencias[i].ExperienciaId;
                    lstExperiencias[i].ExperienciaNombre = this.ObtenerNombre(id);
                }

                return lstExperiencias;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.TendenciaExperienciaPorCliente> TendenciaPorCliente(int tipoDocumento, string nroDocumento)
        {
            try
            {
                var lstTendeciaExperiencia = new List<BE.TendenciaExperienciaPorCliente>();

                var lstTendeciaExperienciaTemp = new DA.Experiencia().TendenciaPorCliente(tipoDocumento, nroDocumento);
                for (int i = 1; i < 5; i++) //Ultimos 4 meses
                {
                    DateTime fecha = DateTime.Now.AddMonths(0 - i);
                    int anho = fecha.Year;
                    int mes = fecha.Month;

                    BE.TendenciaExperienciaPorCliente tendencia =  lstTendeciaExperienciaTemp.Where(x => x.Anho == anho && x.MesId == mes).FirstOrDefault();
                    if (tendencia != null)
                    {

                        switch (tendencia.ExperienciaId)
                        {
                            case 1:
                                tendencia.ExperienciaNombre = "No";
                                break;
                            case 2:
                                tendencia.ExperienciaNombre = "Algo";
                                break;
                            case 3:
                                tendencia.ExperienciaNombre = "Gustó";
                                break;
                            case 4:
                                tendencia.ExperienciaNombre = "Encantó";
                                break;
                            default:
                                tendencia.ExperienciaNombre = "";
                                break;
                        }

                        tendencia.MesNombre = this.ObtenerMes(mes);
                    }
                    else
                    {
                        tendencia = new BE.TendenciaExperienciaPorCliente();
                        tendencia.ExperienciaId = 0;
                        tendencia.ExperienciaNombre = "";
                        tendencia.Anho = anho;
                        tendencia.MesId = mes;
                        tendencia.MesNombre = this.ObtenerMes(mes);
                    }
                    lstTendeciaExperiencia.Add(tendencia);
                }

                return lstTendeciaExperiencia.OrderBy(x => x.Anho).ThenBy(y => y.MesId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string ObtenerNombre(int id)
        {
            string nombre = "";
            switch (id)
            {
                case 1:
                    nombre = "No";
                    break;
                case 2:
                    nombre = "Algo";
                    break;
                case 3:
                    nombre = "Me Gustó";
                    break;
                case 4:
                    nombre = "Me Encantó";
                    break;
                default:
                    break;
            }
            return nombre;
        }

        private string ObtenerMes(int id)
        {
            string mes = "";
            switch (id)
            {
                case 1:
                    mes = "Enero";
                    break;

                case 2:
                    mes = "Febrero";
                    break;

                case 3:
                    mes = "Marzo";
                    break;

                case 4:
                    mes =  "Abril";
                    break;

                case 5:
                    mes = "Mayo";
                    break;

                case 6:
                    mes = "Junio";
                    break;

                case 7:
                    mes = "Julio";
                    break;

                case 8:
                    mes = "Agosto";
                    break;

                case 9:
                    mes = "Septiembre";
                    break;

                case 10:
                    mes = "Octubre";
                    break;

                case 11:
                    mes = "Noviembre";
                    break;

                case 12:
                    mes = "Diciembre";
                    break;

            };

            return mes;
        }
    }
}
