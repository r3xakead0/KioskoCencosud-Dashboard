﻿using System;
using System.Globalization;

namespace DashboardCupones.Library
{
    public class General
    {
        public static DateTime ParseStringToDatetime(string dateString, string formatDate = "dd/MM/yyyy")
        {
            return DateTime.ParseExact(dateString, formatDate, CultureInfo.InvariantCulture);
        }
    }
}