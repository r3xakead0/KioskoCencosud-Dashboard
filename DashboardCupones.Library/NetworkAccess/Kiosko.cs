﻿using Newtonsoft.Json;
using System;
using System.Net.Sockets;
using System.Text;

namespace DashboardCupones.Library.NetworkAccess
{
    public class Kiosko
    {

        const int PORT_NO = 9876;

        private enum Command
        {
            ALL,
            SESSION,
            PRINTER,
            VERSION
        }

        public string Conexion = "NO";
        public string Version = "";
        public string Sesion = "";
        public int Impreso = 0;
        public int Restante = 0;

        public Kiosko(string hostname)
        {
            try
            {

                string jsonAll = this.SendCommand(hostname, Command.ALL);

                if (jsonAll.Length == 0 || jsonAll == "Active")
                    return;

                var jsonDefinition = new { Version = "", Type = "", Number = "", Total = 0, Remaining = 0 };
                var objDefinition = JsonConvert.DeserializeAnonymousType(jsonAll, jsonDefinition);

                if (objDefinition != null)
                {
                    this.Conexion = "SI";
                    this.Version = objDefinition.Version;
                    this.Sesion = objDefinition.Number;
                    this.Impreso = objDefinition.Total;
                    this.Restante = objDefinition.Remaining;
                }
                else
                {
                    this.Conexion = "NO";
                    this.Version = "";
                    this.Sesion = "";
                    this.Impreso = 0;
                    this.Restante = 0;
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string SendCommand(string hostname, Command command)
        {
            try
            {
                string response = "";

                using (var client = new TcpClient())
                {

                    var result = client.BeginConnect(hostname, PORT_NO, null, null);
                    result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(3));

                    if (client.Connected)
                    {
                        byte[] bytesToSend, bytesToRead;
                        NetworkStream nwStream;
                        int bytesRead = 0;

                        string textToSend = command.ToString();
                        bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                        nwStream = client.GetStream();
                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                        bytesToRead = new byte[client.ReceiveBufferSize];
                        bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                        string textToRead = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);

                        response = textToRead.Trim();

                        client.EndConnect(result);
                    }
                   
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static string Restart(string hostname, bool force = false)
        {
            try
            {
                string response = "";

                using (var client = new TcpClient())
                {

                    var result = client.BeginConnect(hostname, PORT_NO, null, null);
                    result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(3));

                    if (client.Connected)
                    {
                        byte[] bytesToSend, bytesToRead;
                        NetworkStream nwStream;
                        int bytesRead = 0;

                        string textToSend = "RESTART" + (force ? " FORCE" : "");
                        bytesToSend = ASCIIEncoding.ASCII.GetBytes(textToSend);

                        nwStream = client.GetStream();
                        nwStream.Write(bytesToSend, 0, bytesToSend.Length);

                        bytesToRead = new byte[client.ReceiveBufferSize];
                        bytesRead = nwStream.Read(bytesToRead, 0, client.ReceiveBufferSize);
                        string textToRead = Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);

                        response = textToRead.Trim();

                        client.EndConnect(result);
                    }

                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
