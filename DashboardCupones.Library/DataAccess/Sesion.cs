﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Library.DataAccess
{
    public class Sesion
    {

        public List<BE.SesionTienda> PorTiendas(DateTime fechaInicio, DateTime fechaFinal)
        {
            try
            {
                var lstBeResumen = new List<BE.SesionTienda>();

                string query = "SELECT	T0.Tienda, " +
                                "COUNT(T0.Tienda) AS Cantidad " +
                                "FROM Clientes_Sesion T0 " +
                                $"WHERE CONVERT(VARCHAR(10),T0.Inicio,112) BETWEEN '{fechaInicio.ToString("yyyyMMdd")}' AND '{fechaFinal.ToString("yyyyMMdd")}' " + 
                                "GROUP BY T0.Tienda " +
                                "ORDER BY[Cantidad] DESC";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var sesion = new BE.SesionTienda()
                    {
                        Tienda = drResumen["Tienda"].ToString(),
                        Cantidad = int.Parse(drResumen["Cantidad"].ToString())
                    };
                    lstBeResumen.Add(sesion);
                }

                return lstBeResumen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.SesionCliente> PorClientes(DateTime fechaInicio, DateTime fechaFinal, int limite = 0)
        {
            try
            {
                string top = limite > 0 ? $"TOP {limite}" : "";
                string[] docNumExonerated = { "012345678", "00000000", "12345678" };

                var lstBeTop = new List<BE.SesionCliente>();

                string query = $"SELECT	{ top } " + 
                            $"T0.NumDoc, " +
                            $"COUNT(T0.NumDoc) AS Cantidad " +
                            $"FROM dbo.Clientes_Sesion T0 " +
                            $"WHERE CONVERT(VARCHAR(10),T0.Inicio,112) BETWEEN '{fechaInicio.ToString("yyyyMMdd")}' AND '{fechaFinal.ToString("yyyyMMdd")}' " +
                            $"AND NOT T0.NumDoc IN('012345678') " +
                            $"GROUP BY T0.NumDoc " +
                            $"ORDER BY[Cantidad] DESC";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var sesion = new BE.SesionCliente()
                    {
                        TipoDocumento = "",
                        NroDocumento = drResumen["NumDoc"].ToString(),
                        Cantidad = int.Parse(drResumen["Cantidad"].ToString())
                    };
                    lstBeTop.Add(sesion);
                }

                return lstBeTop;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.Sesion> Detalle(DateTime fechaInicio, DateTime fechaFinal, string nroDocumento)
        {
            try
            {

                var lstBeSesiones = new List<BE.Sesion>();

                string query = $"SELECT	T0.Tienda, " +
                            $"CONVERT(VARCHAR(10),T0.Inicio,103) AS Fecha, " +
                            $"CONVERT(VARCHAR(8), T0.Inicio, 114) AS Inicio, " +
                            $"CONVERT(VARCHAR(8), T0.Fin, 114) AS Fin, " +
                            $"CONVERT(VARCHAR(8), T0.Fin - T0.Inicio, 114) AS Duracion " +
                            $"FROM dbo.Clientes_Sesion T0 " +
                            $"WHERE CONVERT(VARCHAR(10),T0.Inicio,112) BETWEEN '{fechaInicio.ToString("yyyyMMdd")}' AND '{fechaFinal.ToString("yyyyMMdd")}' " +
                            $"AND T0.NumDoc = '{ nroDocumento }' " +
                            $"ORDER BY T0.Inicio DESC";

                var dtResumen = MssqlHelper.ExecuteQuery(query);

                string formatDate = "dd/MM/yyyy";
                string formatTime = "HH:mm:ss";
                
                foreach (DataRow drResumen in dtResumen.Rows)
                {

                    DateTime fecha = General.ParseStringToDatetime(drResumen["Fecha"].ToString(), formatDate);
                    DateTime inicio = General.ParseStringToDatetime(drResumen["Inicio"].ToString(), formatTime);
                    DateTime fin = General.ParseStringToDatetime(drResumen["Fin"].ToString(), formatTime);
                    DateTime duracion = General.ParseStringToDatetime(drResumen["Duracion"].ToString(), formatTime);

                    var sesion = new BE.Sesion()
                    {
                        Tienda = drResumen["Tienda"].ToString(),
                        Fecha = fecha,
                        Inicio = inicio,
                        Fin = fin,
                        Duracion = duracion
                    };

                    lstBeSesiones.Add(sesion);
                }

                return lstBeSesiones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int PromedioSesionesPorTienda(string codTienda, int dias = 30)
        {
            try
            {

                int promedio = 0;

                DateTime fechaActual = DateTime.Now;

                string query = "SELECT COUNT(T0.Tienda) " +
                            "FROM dbo.Clientes_Sesion T0 " +
                            $"WHERE T0.Tienda = '{ codTienda }' " +
                            "AND CONVERT(VARCHAR(10),T0.Inicio,112) " +
                            $"BETWEEN DATEADD(DAY,-{dias},'{fechaActual.ToString("yyyyMMdd")}') AND '{fechaActual.ToString("yyyyMMdd")}' ";

                var objResult= MssqlHelper.ExecuteScalar(query);

                int cantidad = (int)objResult;

                promedio = cantidad / dias;

                return promedio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int PromedioClientesPorTienda(string codTienda, int dias = 30)
        {
            try
            {
                int promedio = 0;

                DateTime fechaActual = DateTime.Now;

                string query = "SELECT COUNT(DISTINCT T0.NumDoc) " +
                            "FROM dbo.Clientes_Sesion T0 " +
                            $"WHERE T0.Tienda = '{ codTienda }' " +
                            "AND CONVERT(VARCHAR(10),T0.Inicio,112) " +
                            $"BETWEEN DATEADD(DAY,-{dias},'{fechaActual.ToString("yyyyMMdd")}') AND '{fechaActual.ToString("yyyyMMdd")}' ";

                var objResult = MssqlHelper.ExecuteScalar(query);

                int cantidad = (int)objResult;

                promedio = cantidad / dias;

                return promedio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<BE.SesionPorCliente> ResumenPorCliente(int tipoDocumento,
                                            string nroDocumento)
        {
            try
            {
                var lstSesiones = new List<BE.SesionPorCliente>();

                string query = "SELECT T1.Cadena, " +
                            "T0.Tienda, " +
                            "COUNT(T0.NumDoc) AS Cantidad " +
                            "FROM Clientes_Sesion T0 " +
                            "INNER JOIN Tiendas T1 ON T1.Codigo = T0.Tienda " +
                            $"WHERE T0.TipoDoc = {tipoDocumento} " +
                            $"AND T0.NumDoc = '{nroDocumento}' " +
                            "GROUP BY T1.Cadena, T0.Tienda " +
                            "ORDER BY T1.Cadena, T0.Tienda";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var sesion = new BE.SesionPorCliente();

                    sesion.Cadena = drResumen["Cadena"].ToString();
                    sesion.Tienda = drResumen["Tienda"].ToString();
                    sesion.Cantidad = int.Parse(drResumen["Cantidad"].ToString());

                    lstSesiones.Add(sesion);
                }

                return lstSesiones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.TendenciaSesionPorCliente> TendenciaPorCliente(int tipoDocumento,
                                                                    string nroDocumento)
        {
            try
            {
                var lstTendenciaSesiones = new List<BE.TendenciaSesionPorCliente>();

                string query = "SELECT	T0.Anho, " +
                                "T0.Mes, " +
                                "T0.Tienda, " +
                                "T0.Cantidad " +
                                "FROM    Resumen_Clientes_Sesion T0 " +
                                $"WHERE T0.TipoDoc = {tipoDocumento} " +
                                $"AND T0.NumDoc = '{nroDocumento}' " +
                                "ORDER BY T0.Anho, T0.Mes, T0.Tienda";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var tendenciaSesion = new BE.TendenciaSesionPorCliente();

                    tendenciaSesion.Anho = int.Parse(drResumen["Anho"].ToString());
                    tendenciaSesion.MesId = int.Parse(drResumen["Mes"].ToString());
                    tendenciaSesion.Tienda = drResumen["Tienda"].ToString();
                    tendenciaSesion.Cantidad = int.Parse(drResumen["Cantidad"].ToString());

                    lstTendenciaSesiones.Add(tendenciaSesion);
                }

                return lstTendenciaSesiones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
