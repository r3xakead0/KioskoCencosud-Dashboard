﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Library.DataAccess
{
    public class Tienda
    {

        public List<BE.Tienda> Listar()
        {
            try
            {
                var lstTiendas = new List<BE.Tienda>();

                string query = "SELECT T0.Codigo, " +
                            "T0.Nombre, " +
                            "T0.Cadena, " +
                            "T0.Latitud, " +
                            "T0.Longitude, " +
                            "T0.Direccion, " +
                            "T0.Telefono, " +
                            "T0.Jefe " +
                            "FROM Tiendas T0 ";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var tienda = new BE.Tienda();

                    tienda.Codigo = drResumen["Codigo"].ToString();
                    tienda.Nombre = drResumen["Nombre"].ToString();
                    tienda.Empresa = drResumen["Cadena"].ToString();
                    tienda.Latitud = double.Parse(drResumen["Latitud"].ToString());
                    tienda.Longitude = double.Parse(drResumen["Longitude"].ToString());
                    tienda.Direccion = drResumen["Direccion"].ToString();
                    tienda.Telefono = drResumen["Telefono"].ToString();
                    tienda.Jefe = drResumen["Jefe"].ToString();

                    lstTiendas.Add(tienda);
                }

                return lstTiendas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
