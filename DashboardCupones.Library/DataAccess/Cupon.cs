﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Library.DataAccess
{
    public class Cupon
    {

        public List<BE.Cupon> Listar(string nroDocumento, int idGrupo, string ambito, string tienda, int tipo = 0)
        {
            try
            {
                var lstCupones = new List<BE.Cupon>();

                string sp = $"SpMostrarCuponesV2 '{nroDocumento}', {idGrupo}, '{ambito}', '{tienda}', {tipo}";

                var dtResponses = MssqlHelper.ExecuteProcedure(sp);

                foreach (DataRow drResponse in dtResponses.Rows)
                {
                    var cupon = new BE.Cupon();

                    cupon.Id = int.Parse(drResponse["CodCampania"].ToString());

                    cupon.Descuento = drResponse["Descuento"].ToString();
                    cupon.Mensaje1 = drResponse["Mensaje1"].ToString();
                    cupon.Mensaje2 = drResponse["Mensaje2"].ToString();
                    cupon.Mensaje3 = drResponse["Mensaje3"].ToString();
                    cupon.UrlImagen = drResponse["Imagen"].ToString();
                    cupon.Descripcion = drResponse["Descripcion"].ToString();
                    cupon.Prioridad = int.Parse(drResponse["Prioridad"].ToString());
                    cupon.CantidadDisponibles = int.Parse(drResponse["Disponibles"].ToString());
                    cupon.CantidadRedimidos = int.Parse(drResponse["Redimidos"].ToString());
                    cupon.FechaRedencion = drResponse["FechaRedencion"].ToString();
                    cupon.HoraRedencion = drResponse["HoraRedencion"].ToString();

                    lstCupones.Add(cupon);
                }

                return lstCupones;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE.Cupon Obtener(string nroDocumento, int idCupon,  int idGrupo, int tipo = 0)
        {
            BE.Cupon cupon = null;
            try
            { 

                string sp = $"SpObtenerCuponV2 '{nroDocumento}', {idCupon}, {idGrupo}, {tipo}";

                var dtResponses = MssqlHelper.ExecuteProcedure(sp);

                if (dtResponses.Rows.Count == 1)
                {

                    DataRow drResponse = dtResponses.Rows[0];

                    cupon = new BE.Cupon();

                    cupon.Descuento = drResponse["Descuento"].ToString();
                    cupon.Mensaje1 = drResponse["Mensaje1"].ToString();
                    cupon.Mensaje2 = drResponse["Mensaje2"].ToString();
                    cupon.Mensaje3 = drResponse["Mensaje3"].ToString();
                    cupon.Ambito = drResponse["Ambito"].ToString();
                    cupon.TipoLegal = drResponse["TipoLegal"].ToString();
                    cupon.MensajeLegal = drResponse["MensajeLegal"].ToString();
                    cupon.UrlImagen = drResponse["Imagen"].ToString();
                    cupon.CodigoBarra = drResponse["CodigoBarra"].ToString();
                    cupon.CodigoCanal = drResponse["CodigoCanal"].ToString();
                    cupon.FlagSeriado = drResponse["FlagSeriado"].ToString();
                    cupon.BonusSeriado = drResponse["BonusSeriado"].ToString();
                    cupon.CodigoOnline = drResponse["CodigoOnline"].ToString();
                    cupon.Vigencia = drResponse["Vigencia"].ToString();
                    cupon.Tipo = drResponse["Tipo"].ToString();

                }

                return cupon;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    

        public bool Actualizar(BE.Cupon cupon)
        {
            try
            {
                string sql = $"";

                int rowsAffected = MssqlHelper.ExecuteNonQuery(sql);

                return (rowsAffected > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
