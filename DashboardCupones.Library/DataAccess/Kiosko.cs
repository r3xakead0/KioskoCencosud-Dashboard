﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Library.DataAccess
{
    public class Kiosko
    {

        public DateTime FechaHoraUltimoMonitoreo()
        {
            try
            {
                DateTime fechaHora = DateTime.Now;

                string query = "SELECT MAX(FechaHora) FROM Kiosko_Monitoreo";

                object rpta = MssqlHelper.ExecuteScalar(query);
                if (rpta.GetType() != typeof(DBNull))
                    fechaHora = (DateTime)rpta;

                return fechaHora;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime? FechaHoraUltimaConexionPorTienda(string tienda)
        {
            try
            {
                DateTime? fechaHora = null;

                string query = "SELECT MAX(T0.FechaHora) " +
                                "FROM Kiosko_Monitoreo T0 " +
                                "WHERE T0.Conexion = 1 " +
                                $"AND T0.Tienda = '{tienda}' ";

                object rpta = MssqlHelper.ExecuteScalar(query);
                if (rpta.GetType() != typeof(DBNull))
                    fechaHora = (DateTime)rpta;
                else
                {
                    query = "SELECT MIN(T0.FechaHora) " +
                            "FROM Kiosko_Monitoreo T0 ";

                    rpta = MssqlHelper.ExecuteScalar(query);

                    if (rpta.GetType() != typeof(DBNull))
                        return (DateTime)rpta;
                }

                return fechaHora;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.Kiosko> ObtenerMonitoreoPorTienda(string tienda, DateTime fechaHora)
        {
            try
            {
                var lstKioskos = new List<BE.Kiosko>();

                string query = "SELECT T0.Id, T0.Empresa, T0.Tienda, T0.Hostname, T0.Ip, " +
                    "T0.Conexion, T0.Version, T0.Sesion, T0.Impreso, T0.Restante " +
                    "FROM Kiosko_Monitoreo T0 " +
                    $"WHERE T0.Tienda = '{tienda}' " + 
                    $"AND CONVERT(VARCHAR(20), T0.FechaHora, 120) = '{fechaHora.ToString("yyyy-MM-dd HH:mm:ss")}'";

                var dtKioskos = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var kiosko = new BE.Kiosko()
                    {
                        Empresa = drKiosko["Empresa"].ToString(),
                        Tienda = drKiosko["Tienda"].ToString(),
                        Hostname = drKiosko["Hostname"].ToString(),
                        Ip = drKiosko["Ip"].ToString(),
                        Conexion = bool.Parse(drKiosko["Conexion"].ToString()) == true ? "SI" : "NO",
                        Version = drKiosko["Version"].ToString(),
                        Sesion = drKiosko["Sesion"].ToString(),
                        Impreso = int.Parse(drKiosko["Impreso"].ToString()),
                        Restante = int.Parse(drKiosko["Restante"].ToString())
                    };
                    lstKioskos.Add(kiosko);
                }

                return lstKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.Kiosko> ListarMonitoreo(DateTime fechaHora)
        {
            try
            {
                var lstBeKioskos = new List<BE.Kiosko>();

                string query = "SELECT Id, Empresa, Tienda, Hostname, Ip, " +
                    "Conexion, Version, Sesion, Impreso, Restante " +
                    "FROM Kiosko_Monitoreo " +
                    $"WHERE  CONVERT(VARCHAR(20), FechaHora, 120) = '{fechaHora.ToString("yyyy-MM-dd HH:mm:ss")}'";

                var dtKioskos = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var kiosko = new BE.Kiosko()
                    {
                        Empresa = drKiosko["Empresa"].ToString(),
                        Tienda = drKiosko["Tienda"].ToString(),
                        Hostname = drKiosko["Hostname"].ToString(),
                        Ip = drKiosko["Ip"].ToString(),
                        Conexion = bool.Parse(drKiosko["Conexion"].ToString()) == true ? "SI" : "NO",
                        Version = drKiosko["Version"].ToString(),
                        Sesion = drKiosko["Sesion"].ToString(),
                        Impreso = int.Parse(drKiosko["Impreso"].ToString()),
                        Restante = int.Parse(drKiosko["Restante"].ToString())
                    };
                    lstBeKioskos.Add(kiosko);
                }

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarMonitoreo(BE.Kiosko beKiosko, DateTime fechaHora)
        {
            try
            {
                bool rpta = false;

                if (beKiosko != null)
                {
                    string strFechaHora = fechaHora.ToString("yyyy-MM-dd HH:mm:ss");

                    string query = "";
                    query += "INSERT INTO Kiosko_Monitoreo (Empresa,Tienda, Hostname, Ip, Conexion, Version, Sesion, Impreso, Restante, FechaHora) ";
                    query += $"VALUES ('{beKiosko.Empresa}','{beKiosko.Tienda}', '{beKiosko.Hostname}', '{beKiosko.Ip}', '{beKiosko.Conexion.Equals("SI")}', '{beKiosko.Version}', '{beKiosko.Sesion}', {beKiosko.Impreso}, {beKiosko.Restante}, CAST('{strFechaHora}' AS DATETIME))";

                    int rowsAffected = MssqlHelper.ExecuteNonQuery(query);

                    rpta = rowsAffected > 0;
                }

                return rpta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.Kiosko> Listar()
        {
            try
            {
                var lstBeKioskos = new List<BE.Kiosko>();

                string query = "SELECT DISTINCT T0.Compania, T0.Tienda, T0.hostname " +
                    "FROM config T0 " +
                    "WHERE T0.Activo = 1 " +
                    "ORDER BY T0.Compania, T0.Tienda";

                var dtKioskos = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var kiosko = new BE.Kiosko()
                    {
                        Empresa = drKiosko["Compania"].ToString(),
                        Tienda = drKiosko["Tienda"].ToString(),
                        Hostname = drKiosko["hostname"].ToString(),
                        Ip = "",
                        Conexion = "NO",
                        Version = "",
                        Sesion = "",
                        Impreso = 0,
                        Restante = 0
                    };
                    lstBeKioskos.Add(kiosko);
                }

                return lstBeKioskos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.General> ListaSimpleEmpresas()
        {
            try
            {
                var lstBeGeneral = new List<BE.General>();

                string query = "SELECT DISTINCT Compania FROM config ORDER BY Compania";
                var dtKioskos = MssqlHelper.ExecuteQuery(query);
               foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var general = new BE.General()
                    {
                        Codigo = drKiosko["Compania"].ToString(),
                        Nombre = drKiosko["Compania"].ToString()
                    };
                    lstBeGeneral.Add(general);
                }

                return lstBeGeneral;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.General> ListaSimpleTiendas(string empresa)
        {
            try
            {
                var lstBeGeneral = new List<BE.General>();

                string query = $"SELECT DISTINCT Tienda FROM config WHERE Compania = '{empresa}' ORDER BY Tienda";
                var dtKioskos = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var general = new BE.General()
                    {
                        Codigo = drKiosko["Tienda"].ToString(),
                        Nombre = drKiosko["Tienda"].ToString()
                    };
                    lstBeGeneral.Add(general);
                }

                return lstBeGeneral;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BE.General> ListaSimpleHostnames(string tienda)
        {
            try
            {
                var lstBeGeneral = new List<BE.General>();

                string query = $"SELECT DISTINCT hostname FROM config WHERE Tienda = '{tienda}' ORDER BY hostname";
                var dtKioskos = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drKiosko in dtKioskos.Rows)
                {
                    var general = new BE.General()
                    {
                        Codigo = drKiosko["hostname"].ToString(),
                        Nombre = drKiosko["hostname"].ToString()
                    };
                    lstBeGeneral.Add(general);
                }

                return lstBeGeneral;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
