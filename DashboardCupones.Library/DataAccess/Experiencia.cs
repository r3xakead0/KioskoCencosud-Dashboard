﻿using System.Collections.Generic;
using System.Data;
using System;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Library.DataAccess
{
    public class Experiencia
    {

        public List<BE.Experiencia> Resumen(DateTime fechaInicio,
                                            DateTime fechaFinal,
                                            string empresa = "",
                                            string tienda = "", 
                                            string hostname = "")
        {
            try
            {
                var lstBeResumen = new List<BE.Experiencia>();

                string query = "SELECT T0.Experiencia, " +
                            "COUNT(T0.Experiencia) AS Cantidad " +
                            "FROM Clientes_Experiencia T0 " +
                            "INNER JOIN Config T1 ON T1.Tienda = T0.Tienda " +
                            $"WHERE CONVERT(VARCHAR(10),T0.FechaRegistro,112) BETWEEN '{fechaInicio.ToString("yyyyMMdd")}' AND '{fechaFinal.ToString("yyyyMMdd")}' ";

                if (empresa.Length > 0)
                    query += $"AND T1.Compania = '{empresa}' ";

                if(tienda.Length > 0)
                    query += $"AND T0.Tienda = '{tienda}' ";

                if (hostname.Length > 0)
                    query += $"AND T1.Hostname = '{hostname}' ";

                query += "GROUP BY T0.Experiencia " +
                        "ORDER BY T0.Experiencia";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var experiencia = new BE.Experiencia()
                    {
                        Id = int.Parse(drResumen["Experiencia"].ToString()),
                        Nombre = "",
                        Cantidad = int.Parse(drResumen["Cantidad"].ToString())
                    };
                    lstBeResumen.Add(experiencia);
                }

                return lstBeResumen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.TendenciaExperienciaPorCliente> TendenciaPorCliente(int tipoDocumento,
                                                                    string nroDocumento)
        {
            try
            {
                var lstTendenciaExperiencia = new List<BE.TendenciaExperienciaPorCliente>();

                string query = "SELECT	T0.Anho, " +
                                "T0.Mes, " + 
                                "T0.Experiencia " +
                                "FROM    Resumen_Clientes_Experiencia T0 " +
                                $"WHERE T0.TipoDoc = {tipoDocumento} " +
                                $"AND T0.NumDoc = '{nroDocumento}' " +
                                "ORDER BY T0.Anho, T0.Mes";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var tendenciaExperiencia = new BE.TendenciaExperienciaPorCliente();

                    tendenciaExperiencia.Anho = int.Parse(drResumen["Anho"].ToString());
                    tendenciaExperiencia.MesId = int.Parse(drResumen["Mes"].ToString());
                    tendenciaExperiencia.ExperienciaId = int.Parse(drResumen["Experiencia"].ToString());
                    tendenciaExperiencia.ExperienciaNombre = "";

                    lstTendenciaExperiencia.Add(tendenciaExperiencia);
                }

                return lstTendenciaExperiencia;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE.ExperienciaPorCliente> ResumenPorCliente(int tipoDocumento,
                                            string nroDocumento)
        {
            try
            {
                var lstBeResumen = new List<BE.ExperienciaPorCliente>();

                string query = "SELECT	T1.Cadena, " +
                                "T0.Tienda, " +
                                "AVG(T0.Experiencia) AS Experiencia " +
                                "FROM Clientes_Experiencia T0 " +
                                "INNER JOIN Tiendas T1 ON T1.Codigo = T0.Tienda " +
                                $"WHERE T0.TipoDoc = {tipoDocumento} " +
                                $"AND T0.NumDoc = '{nroDocumento}' " +
                                "GROUP BY T1.Cadena, T0.Tienda " +
                                "ORDER BY T1.Cadena, T0.Tienda";

                var dtResumen = MssqlHelper.ExecuteQuery(query);
                foreach (DataRow drResumen in dtResumen.Rows)
                {
                    var experiencia = new BE.ExperienciaPorCliente()
                    {
                        Cadena = drResumen["Cadena"].ToString(),
                        Tienda = drResumen["Tienda"].ToString(),
                        ExperienciaId = int.Parse(drResumen["Experiencia"].ToString()),
                        ExperienciaNombre = ""
                    };
                    lstBeResumen.Add(experiencia);
                }

                return lstBeResumen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int PromedioPorTienda(string codTienda, int dias = 30)
        {
            try
            {
                DateTime fechaActual = DateTime.Now;

                string query = "SELECT ISNULL(AVG(T0.Experiencia),0) " +
                            "FROM dbo.Clientes_Experiencia T0 " +
                            $"WHERE T0.Tienda = '{ codTienda }' " +
                            "AND CONVERT(VARCHAR(10),T0.FechaRegistro,112) " +
                            $"BETWEEN DATEADD(DAY,-{dias},'{fechaActual.ToString("yyyyMMdd")}') AND '{fechaActual.ToString("yyyyMMdd")}' ";

                var objResult = MssqlHelper.ExecuteScalar(query);

                return (int)objResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
