﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class TendenciaExperienciaPorCliente
    {
        public int Anho { get; set; } = 0;
        public int MesId { get; set; } = 0;
        public string MesNombre { get; set; } = "";
        public int ExperienciaId { get; set; } = 0;
        public string ExperienciaNombre { get; set; } = "";
    }
}
