﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class ExperienciaPorCliente
    {
        public string Cadena { get; set; } = "";
        public string Tienda { get; set; } = "";
        public int ExperienciaId { get; set; } = 0;
        public string ExperienciaNombre { get; set; } = "";
    }
}
