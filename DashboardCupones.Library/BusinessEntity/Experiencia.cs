﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class Experiencia
    {
        public int Id { get; set; } = 0;
        public string Nombre { get; set; } = "";
        public int Cantidad { get; set; } = 0;
        public double Porcentaje { get; set; } = 0.0;
    }
}
