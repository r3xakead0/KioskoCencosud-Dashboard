﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class TendenciaSesionPorCliente
    {
        public int Anho { get; set; } = 0;
        public int MesId { get; set; } = 0;
        public string MesNombre { get; set; } = "";
        public string Tienda { get; set; } = "";
        public int Cantidad { get; set; } = 0;
    }
}
