﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class Tienda : General
    {
        public string Empresa { get; set; } = "";
        public double Latitud { get; set; } = 0.0;
        public double Longitude { get; set; } = 0.0;
        public string Direccion { get; set; } = "";
        public string Telefono { get; set; } = "NO";
        public string Jefe { get; set; } = "";

        public int Kioskos { get; set; } = 0;
        public bool Conectado { get; set; } = false;

        public int PromedioSesiones { get; set; } = 0;
        public int PromedioClientes { get; set; } = 0;
        public int PromedioSatisfaccion { get; set; } = 0;

        public int DiasInactividad { get; set; } = 0;
    }
}
