﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class SesionTienda
    {
        public string Tienda { get; set; } = "";
        public int Cantidad { get; set; } = 0;
        public double Porcentaje { get; set; } = 0.0;
    }

}
