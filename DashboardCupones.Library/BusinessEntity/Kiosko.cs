﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class Kiosko
    {
        public int Id { get; set; } = 0;
        public string Empresa { get; set; } = "";
        public string Tienda { get; set; } = "";
        public string Hostname { get; set; } = "";
        public string Ip { get; set; } = "";
        public string Conexion { get; set; } = "NO";
        public string Version { get; set; } = "";
        public string Sesion { get; set; } = "";
        public int Impreso { get; set; } = 0;
        public int Restante { get; set; } = 0;
    }
}
