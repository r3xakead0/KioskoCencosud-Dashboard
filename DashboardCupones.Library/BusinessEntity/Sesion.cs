﻿using System;

namespace DashboardCupones.Library.BusinessEntity
{
    public class Sesion
    {
        public string Tienda { get; set; } 
        public DateTime Fecha { get; set; } 
        public DateTime Inicio { get; set; } 
        public DateTime Fin { get; set; } 
        public DateTime Duracion { get; set; } 
    }
}
