﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class Cupon
    {
        public int Id { get; set; } //CodCampania

        //Datos de Impresion
        public string Descuento { get; set; }
        public string Mensaje1 { get; set; }
        public string Mensaje2 { get; set; }
        public string Mensaje3 { get; set; }
        public string Ambito { get; set; }
        public string TipoLegal { get; set; }
        public string MensajeLegal { get; set; }
        public string UrlImagen { get; set; } //Imagen
        public string CodigoBarra { get; set; }
        public string CodigoCanal { get; set; }
        public string FlagSeriado { get; set; }
        public string BonusSeriado { get; set; }
        public string CodigoOnline { get; set; }
        public string Vigencia { get; set; }
        public string Tipo { get; set; }

        //Datos de Lista
        public string Descripcion { get; set; }
        public int Prioridad { get; set; }
        public int CantidadDisponibles { get; set; }
        public int CantidadRedimidos { get; set; }
        public string FechaRedencion { get; set; }
        public string HoraRedencion { get; set; }
    }
}
