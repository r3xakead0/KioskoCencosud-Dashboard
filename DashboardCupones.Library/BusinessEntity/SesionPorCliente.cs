﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class SesionPorCliente
    {
        public string Cadena { get; set; } = "";
        public string Tienda { get; set; } = "";
        public int Cantidad { get; set; } = 0;
    }

}
