﻿namespace DashboardCupones.Library.BusinessEntity
{
    public class SesionCliente
    {
        public string TipoDocumento { get; set; } = "";
        public string NroDocumento { get; set; } = "";
        public int Cantidad { get; set; } = 0;
    }

}
