﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL = DashboardCupones.Library.BusinessLogic;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Web.Controllers
{
    public class KioskoController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        // GET: Monitoreo

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sidx">sortname</param>
        /// <param name="sord">sortorder</param>
        /// <param name="page">page</param>
        /// <param name="rows">rowNum</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Monitoreo(string sidx, string sord, int page, int rows)
        {
            var blKioskos = new BL.Kiosko();

            DateTime fechaHora = blKioskos.UltimoMonitoreo();

            var results = blKioskos.Monitoreo(fechaHora).AsQueryable();

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            int totalRecords = results.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);
            
            if (sord.ToUpper() == "DESC")
            {
                switch (sidx.ToUpper())
                {
                    case "EMPRESA":
                        results = results.OrderByDescending(s => s.Empresa);
                        break;
                    case "TIENDA":
                        results = results.OrderByDescending(s => s.Tienda);
                        break;
                    case "HOSTNAME":
                        results = results.OrderByDescending(s => s.Hostname);
                        break;
                    case "IP":
                        results = results.OrderByDescending(s => s.Ip);
                        break;
                    case "CONEXION":
                        results = results.OrderByDescending(s => s.Conexion);
                        break;
                    case "VERSION":
                        results = results.OrderByDescending(s => s.Version);
                        break;
                    case "SESION":
                        results = results.OrderByDescending(s => s.Sesion);
                        break;
                    case "IMPRESO":
                        results = results.OrderByDescending(s => s.Impreso);
                        break;
                    case "RESTANTE":
                        results = results.OrderByDescending(s => s.Restante);
                        break;
                    default:
                        results = results.OrderByDescending(s => s.Id);
                        break;
                }
            }
            else
            {
                switch (sidx.ToUpper())
                {
                    case "EMPRESA":
                        results = results.OrderBy(s => s.Empresa);
                        break;
                    case "TIENDA":
                        results = results.OrderBy(s => s.Tienda);
                        break;
                    case "HOSTNAME":
                        results = results.OrderBy(s => s.Hostname);
                        break;
                    case "IP":
                        results = results.OrderBy(s => s.Ip);
                        break;
                    case "CONEXION":
                        results = results.OrderBy(s => s.Conexion);
                        break;
                    case "VERSION":
                        results = results.OrderBy(s => s.Version);
                        break;
                    case "SESION":
                        results = results.OrderBy(s => s.Sesion);
                        break;
                    case "IMPRESO":
                        results = results.OrderBy(s => s.Impreso);
                        break;
                    case "RESTANTE":
                        results = results.OrderBy(s => s.Restante);
                        break;
                    default:
                        results = results.OrderBy(s => s.Id);
                        break;
                }
            }
            results = results.Skip(pageIndex * pageSize).Take(pageSize);

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = results
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Resumen()
        {
            var blKioskos = new BL.Kiosko();

            DateTime fechaHora = blKioskos.UltimoMonitoreo();

            var lstKioskos = blKioskos.Monitoreo(fechaHora);

            var resumen = lstKioskos
                        .GroupBy(a => a.Conexion)
                        .Select(a => new
                        {
                            Conexion = a.Key,
                            Total = a.Count()
                        });

            var jsonData = new
            {
                grafico = resumen,
                fechahora = fechaHora.ToString("dd/MM/yyyy HH:mm:ss")
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UltimaActualizacion()
        {
            var blKioskos = new BL.Kiosko();

            DateTime fechaHora = blKioskos.UltimoMonitoreo();

            return Json(fechaHora, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void Reiniciar()
        {
            var blKioskos = new BL.Kiosko();
        }
    }
}