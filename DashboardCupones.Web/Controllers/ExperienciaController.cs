﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL = DashboardCupones.Library.BusinessLogic;
using BE = DashboardCupones.Library.BusinessEntity;

namespace DashboardCupones.Web.Controllers
{
    public class ExperienciaController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Resumen(string inicio, string final, string empresa = "", string tienda = "")
        {
            int anho = DateTime.Now.Year;
            int mes = DateTime.Now.Month;

            DateTime fechaInicio;
            if (DateTime.TryParseExact(inicio, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaInicio) == false)
                fechaInicio = new DateTime(anho, mes, 1);

            DateTime fechaFinal;
            if (DateTime.TryParseExact(final, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaFinal) == false)
                fechaFinal = new DateTime();

            var lstExperiencias = new BL.Experiencia().Resumen(fechaInicio, fechaFinal, empresa, tienda, "");

            var jsonData = lstExperiencias.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Empresas()
        {
            var lstEmpresas = new BL.Kiosko().ListaSimpleEmpresas();

            var jsonData = lstEmpresas.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult Tiendas(string empresa = "")
        {
            var lstTiendas = new BL.Kiosko().ListaSimpleTiendas(empresa);

            var jsonData = lstTiendas.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ResumenPorCliente(int tipDoc, string nroDoc)
        {
            var lstExperiencias = new BL.Experiencia().ResumenPorCliente(tipDoc, nroDoc);

            var jsonData = lstExperiencias.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult TendenciaPorCliente(int tipDoc, string nroDoc)
        {
            var lstExperiencias = new BL.Experiencia().TendenciaPorCliente(tipDoc, nroDoc);

            var jsonData = lstExperiencias.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}