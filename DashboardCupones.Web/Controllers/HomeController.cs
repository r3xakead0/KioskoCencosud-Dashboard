﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL = DashboardCupones.Library.BusinessLogic;

namespace DashboardCupones.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult ListarTiendas()
        {
            var lstKioskos = new BL.Tienda().Listar();

            var jsonData = lstKioskos.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}