﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BL = DashboardCupones.Library.BusinessLogic;
using BE = DashboardCupones.Library.BusinessEntity;
using System.Globalization;

namespace DashboardCupones.Web.Controllers
{
    public class SesionController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult ResumenPorTiendas(string inicio, string final)
        {
            int anho = DateTime.Now.Year;
            int mes = DateTime.Now.Month;

            DateTime fechaInicio;
            if (DateTime.TryParseExact(inicio, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaInicio) == false)
                fechaInicio = new DateTime(anho, mes, 1);

            DateTime fechaFinal;
            if (DateTime.TryParseExact(final, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaFinal) == false)
                fechaFinal = new DateTime();

            var lstSesiones = new BL.Sesion().PorTiendas(fechaInicio, fechaFinal);

            var jsonData = lstSesiones.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ResumenPorClientes(string inicio, string final, 
                                            string sidx, string sord, int page, int rows)
        {

            int anho = DateTime.Now.Year;
            int mes = DateTime.Now.Month;

            DateTime fechaInicio;
            if (DateTime.TryParseExact(inicio, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaInicio) == false)
                fechaInicio = new DateTime(anho, mes, 1);

            DateTime fechaFinal;
            if (DateTime.TryParseExact(final, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaFinal) == false)
                fechaFinal = new DateTime();

            var lstSesiones = new BL.Sesion().TopClientes(fechaInicio, fechaFinal, 100);

            var results = lstSesiones.AsQueryable();

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            int totalRecords = results.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

            if (sord.ToUpper() == "DESC")
            {
                switch (sidx.ToUpper())
                {
                    case "TIPODOCUMENTO":
                        results = results.OrderByDescending(s => s.TipoDocumento);
                        break;
                    case "NRODOCUMENTO":
                        results = results.OrderByDescending(s => s.NroDocumento);
                        break;
                    default:
                        results = results.OrderByDescending(s => s.Cantidad);
                        break;
                }
            }
            else
            {
                switch (sidx.ToUpper())
                {
                    case "TIPODOCUMENTO":
                        results = results.OrderBy(s => s.TipoDocumento);
                        break;
                    case "NRODOCUMENTO":
                        results = results.OrderBy(s => s.NroDocumento);
                        break;
                    default:
                        results = results.OrderBy(s => s.Cantidad);
                        break;
                }
            }
            results = results.Skip(pageIndex * pageSize).Take(pageSize);

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = results
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SesionesPorCliente(string inicio, string final, 
                                            string sidx, string sord, int page, int rows, string numdoc)
        {

            int anho = DateTime.Now.Year;
            int mes = DateTime.Now.Month;

            DateTime fechaInicio;
            if (DateTime.TryParseExact(inicio, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaInicio) == false)
                fechaInicio = new DateTime(anho, mes, 1);

            DateTime fechaFinal;
            if (DateTime.TryParseExact(final, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fechaFinal) == false)
                fechaFinal = new DateTime();

            var lstSesiones = new BL.Sesion().Destalle(fechaInicio, fechaFinal, numdoc);

            var results = lstSesiones.AsQueryable();

            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;

            int totalRecords = results.Count();
            var totalPages = (int)Math.Ceiling((float)totalRecords / (float)rows);

            if (sord.ToUpper() == "DESC")
            {
                switch (sidx.ToUpper())
                {
                    case "TIENDA":
                        results = results.OrderByDescending(s => s.Tienda);
                        break;
                    case "INICIO":
                        results = results.OrderByDescending(s => s.Inicio);
                        break;
                    case "FIN":
                        results = results.OrderByDescending(s => s.Fin);
                        break;
                    case "DURACION":
                        results = results.OrderByDescending(s => s.Duracion);
                        break;
                    default: //Fecha
                        results = results.OrderByDescending(s => s.Fecha);
                        break;
                }
            }
            else
            {
                switch (sidx.ToUpper())
                {
                    case "TIENDA":
                        results = results.OrderBy(s => s.Tienda);
                        break;
                    case "INICIO":
                        results = results.OrderBy(s => s.Inicio);
                        break;
                    case "FIN":
                        results = results.OrderBy(s => s.Fin);
                        break;
                    case "DURACION":
                        results = results.OrderBy(s => s.Duracion);
                        break;
                    default: //Fecha
                        results = results.OrderBy(s => s.Fecha);
                        break;
                }
            }
            results = results.Skip(pageIndex * pageSize).Take(pageSize);

            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecords,
                rows = results
            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ResumenPorCliente(int tipDoc, string nroDoc)
        {
            var lstSesiones = new BL.Sesion().ResumenPorCliente(tipDoc, nroDoc);

            var jsonData = lstSesiones.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult TendenciaPorCliente(int tipDoc, string nroDoc)
        {
            var lstSesiones = new BL.Sesion().TendenciaPorCliente(tipDoc, nroDoc);

            var jsonData = lstSesiones.AsQueryable();

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }

}